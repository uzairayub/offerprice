package com.uzair.offer.price.Models.Inbox;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Company implements Serializable {

	@SerializedName("company_id")
	private int companyId;

	@SerializedName("rating")
	private String rating;

	@SerializedName("company_email")
	private String companyEmail;

	@SerializedName("company_picture")
	private String companyPicture;

	@SerializedName("company_joindate")
	private String companyJoindate;

	@SerializedName("company_address")
	private String companyAddress;

	@SerializedName("location_id")
	private String locationId;

	@SerializedName("token")
	private String token;

	@SerializedName("location_name")
	private String locationName;

	@SerializedName("company_name")
	private String companyName;

	@SerializedName("arabic_name")
	private String arabicName;

	@SerializedName("company_password")
	private String companyPassword;

	@SerializedName("company_about")
	private String companyAbout;

	public void setCompanyId(int companyId){
		this.companyId = companyId;
	}

	public int getCompanyId(){
		return companyId;
	}

	public void setRating(String rating){
		this.rating = rating;
	}

	public String getRating(){
		return rating;
	}

	public void setCompanyEmail(String companyEmail){
		this.companyEmail = companyEmail;
	}

	public String getCompanyEmail(){
		return companyEmail;
	}

	public void setCompanyPicture(String companyPicture){
		this.companyPicture = companyPicture;
	}

	public String getCompanyPicture(){
		return companyPicture;
	}

	public void setCompanyJoindate(String companyJoindate){
		this.companyJoindate = companyJoindate;
	}

	public String getCompanyJoindate(){
		return companyJoindate;
	}

	public void setCompanyAddress(String companyAddress){
		this.companyAddress = companyAddress;
	}

	public String getCompanyAddress(){
		return companyAddress;
	}

	public void setLocationId(String locationId){
		this.locationId = locationId;
	}

	public String getLocationId(){
		return locationId;
	}

	public void setToken(String token){
		this.token = token;
	}

	public String getToken(){
		return token;
	}

	public void setLocationName(String locationName){
		this.locationName = locationName;
	}

	public String getLocationName(){
		return locationName;
	}

	public void setCompanyName(String companyName){
		this.companyName = companyName;
	}

	public String getCompanyName(){
		return companyName;
	}

	public void setArabicName(String arabicName){
		this.arabicName = arabicName;
	}

	public String getArabicName(){
		return arabicName;
	}

	public void setCompanyPassword(String companyPassword){
		this.companyPassword = companyPassword;
	}

	public String getCompanyPassword(){
		return companyPassword;
	}

	public void setCompanyAbout(String companyAbout){
		this.companyAbout = companyAbout;
	}

	public String getCompanyAbout(){
		return companyAbout;
	}

	@Override
 	public String toString(){
		return 
			"Company{" + 
			"company_id = '" + companyId + '\'' + 
			",rating = '" + rating + '\'' + 
			",company_email = '" + companyEmail + '\'' + 
			",company_picture = '" + companyPicture + '\'' + 
			",company_joindate = '" + companyJoindate + '\'' + 
			",company_address = '" + companyAddress + '\'' + 
			",location_id = '" + locationId + '\'' + 
			",token = '" + token + '\'' + 
			",location_name = '" + locationName + '\'' + 
			",company_name = '" + companyName + '\'' + 
			",arabic_name = '" + arabicName + '\'' + 
			",company_password = '" + companyPassword + '\'' + 
			",company_about = '" + companyAbout + '\'' + 
			"}";
		}
}