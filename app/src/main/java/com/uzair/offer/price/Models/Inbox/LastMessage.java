package com.uzair.offer.price.Models.Inbox;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class LastMessage implements Serializable {

	@SerializedName("thread_id")
	private String threadId;

	@SerializedName("sent_by")
	private String sentBy;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("files")
	private List<FilesItem> files;

	@SerializedName("id")
	private int id;

	@SerializedName("text")
	private String text;

	public void setThreadId(String threadId){
		this.threadId = threadId;
	}

	public String getThreadId(){
		return threadId;
	}

	public void setSentBy(String sentBy){
		this.sentBy = sentBy;
	}

	public String getSentBy(){
		return sentBy;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setFiles(List<FilesItem> files){
		this.files = files;
	}

	public List<FilesItem> getFiles(){
		return files;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		return text;
	}

	@Override
 	public String toString(){
		return 
			"LastMessage{" + 
			"thread_id = '" + threadId + '\'' + 
			",sent_by = '" + sentBy + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",files = '" + files + '\'' + 
			",id = '" + id + '\'' + 
			",text = '" + text + '\'' + 
			"}";
		}
}