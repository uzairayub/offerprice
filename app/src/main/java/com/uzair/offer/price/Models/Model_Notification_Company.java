package com.uzair.offer.price.Models;


public class Model_Notification_Company {

    private String notificaationid, entertain, companyid, customerid, message, image, companyname, customername ;

    public String getEntertain() {
        return entertain;
    }

    public void setEntertain(String entertain) {
        this.entertain = entertain;
    }

    public String getNotificaationid() {
        return notificaationid;
    }

    public void setNotificaationid(String notificaationid) {
        this.notificaationid = notificaationid;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getCompanyid() {
        return companyid;
    }

    public void setCompanyid(String companyid) {
        this.companyid = companyid;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
