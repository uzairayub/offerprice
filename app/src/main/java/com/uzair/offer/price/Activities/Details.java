package com.uzair.offer.price.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.uzair.offer.price.R;

public class Details extends AppCompatActivity {

    private ImageView imageView;
    private TextView txttitle, txtweight, txtcolor,txtnumber, txtunit,
            txtconsumption, txtinch, txtthickness, txttype, txtheight,
            txtlength, txtdiameter, txtsize, txtwatt, txtampre;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        initial();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void initial()
    {
        Toolbar toolbar = (Toolbar)findViewById(R.id.details_toolbar);
        toolbar.setTitle(getIntent().getExtras().getString("name"));
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        imageView = (ImageView)findViewById(R.id.details_imageview);
        txttitle = (TextView)findViewById(R.id.details_title);
        txtweight = (TextView)findViewById(R.id.details_txtweight);
        txtcolor = (TextView)findViewById(R.id.details_txtcolor);
        txtnumber = (TextView)findViewById(R.id.details_txtnumber);
        txtunit = (TextView)findViewById(R.id.details_txtunit);
        txtconsumption = (TextView)findViewById(R.id.details_txtconsumption);
        txtinch = (TextView)findViewById(R.id.details_txtinch);
        txtthickness = (TextView)findViewById(R.id.details_txtthickness);
        txttype = (TextView)findViewById(R.id.details_txttype);
        txtheight = (TextView)findViewById(R.id.details_txtheight);
        txtlength = (TextView)findViewById(R.id.details_txtlenght);
        txtdiameter = (TextView)findViewById(R.id.details_txtdiameter);
        txtsize = (TextView)findViewById(R.id.details_txtsize);
        txtwatt = (TextView)findViewById(R.id.details_txtwatt);
        txtampre = (TextView)findViewById(R.id.details_txtampre);
        getdata();

    }

    private void getdata()
    {
        String url = "http://www.arzsaar.com/materialpictures/"+getIntent().getExtras().getString("image");
        Picasso.get().load(url).placeholder(R.drawable.ic_photo_placeholder).error(R.drawable.ic_photo_placeholder).into(imageView);
        txttitle.setText(getIntent().getExtras().getString("name"));
        txtweight.setText(getIntent().getExtras().getString("weight"));
        txtcolor.setText(getIntent().getExtras().getString("color"));
        txtnumber.setText(getIntent().getExtras().getString("number"));
        txtunit.setText(getIntent().getExtras().getString("unit"));
        txtconsumption.setText(getIntent().getExtras().getString("consumption"));
        txtinch.setText(getIntent().getExtras().getString("inch"));
        txtthickness.setText(getIntent().getExtras().getString("thickness"));
        txttype.setText(getIntent().getExtras().getString("type"));
        txtheight.setText(getIntent().getExtras().getString("height"));
        txtlength.setText(getIntent().getExtras().getString("length"));
        txtdiameter.setText(getIntent().getExtras().getString("diameter"));
        txtsize.setText(getIntent().getExtras().getString("size"));
        txtwatt.setText(getIntent().getExtras().getString("watt"));
        txtampre.setText(getIntent().getExtras().getString("ampre"));
    }
}
