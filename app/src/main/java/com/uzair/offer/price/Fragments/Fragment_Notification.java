package com.uzair.offer.price.Fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.uzair.offer.price.Adapters.Adapter_Notification_Company;
import com.uzair.offer.price.Adapters.Adapter_Notification_Customer;
import com.uzair.offer.price.Interfaces.InterfaceAcceptOffer;
import com.uzair.offer.price.Models.Model_Notification_Company;
import com.uzair.offer.price.R;
import com.uzair.offer.price.Utils.Constants;

import org.json.JSONArray;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import dmax.dialog.SpotsDialog;
//import pl.aprilapps.easyphotopicker.EasyImage;

public class Fragment_Notification extends Fragment implements SwipeRefreshLayout.OnRefreshListener, InterfaceAcceptOffer{

    private RecyclerView recyclerView;
    private SwipeRefreshLayout refresher;
    private View view;
    private SharedPreferences sharedPreferences;
    private ArrayList<Model_Notification_Company> companylist;
    private Adapter_Notification_Company companyAdapter;
    private ViewGroup dialogTakePhotoLayout;
    private ImageView dialogimgSelected;
    private File selectedImage;
    private AlertDialog laodingdialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_notification,container,false);
        initial();
        return view;
    }

    private void initial()
    {
        sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        refresher = (SwipeRefreshLayout) view.findViewById(R.id.notification_refresher);
        refresher.setOnRefreshListener(this);
        recyclerView = (RecyclerView)view.findViewById(R.id.notification_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        loadnitifications();
    }

    @Override
    public void onRefresh()
    {
        loadnitifications();
    }

    private void loadnitifications()
    {
        if(iscompany())
        {
            refresher.setRefreshing(true);
            Ion.with(getContext())
                    .load(Constants.BASE_URL+"show_unEntertain.php")
                    .setMultipartParameter("id",sharedPreferences.getString("id","0"))
                    .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
                @Override
                public void onCompleted(Exception e, JsonObject result)
                {
                    refresher.setRefreshing(false);
                    if(e == null)
                    {
                        if(result.get("status").getAsString().equalsIgnoreCase("1"))
                        {
                            JsonArray notifications = result.get("history").getAsJsonArray();
                            loadcompanynotifications(notifications);
                        }
                    }
                    else
                    {

                    }
                }
            });
        }
        else if(!iscompany())
        {
            refresher.setRefreshing(true);
            Ion.with(getContext())
                    .load(Constants.BASE_URL+"show_allRequests.php")
                    .setMultipartParameter("id",sharedPreferences.getString("id","0"))
                    .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
                @Override
                public void onCompleted(Exception e, JsonObject result)
                {
                    refresher.setRefreshing(false);
                    if(e == null)
                    {
                        if(result.get("status").getAsString().equalsIgnoreCase("1"))
                        {
                            JsonArray notifications = result.get("history").getAsJsonArray();
                            loadcustomernotifications(notifications);
                        }
                    }
                    else
                    {
                        String message = getResources().getString(R.string.connectionerror);
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void loadcompanynotifications(JsonArray notifications)
    {
        companylist = new ArrayList<>();
        Model_Notification_Company model;
        for(int i = 0; i < notifications.size(); i++)
        {
            JsonObject note = notifications.get(i).getAsJsonObject();
            model = new Model_Notification_Company();
            if(!note.get("id").isJsonNull())model.setNotificaationid(note.get("id").getAsString());
            if(!note.get("company_id").isJsonNull())model.setCompanyid(note.get("company_id").getAsString());
            if(!note.get("company_name").isJsonNull())model.setCompanyname(note.get("company_name").getAsString());
            if(!note.get("customer_id").isJsonNull())model.setCustomerid(note.get("customer_id").getAsString());
            if(!note.get("customer_name").isJsonNull())model.setCustomername(note.get("customer_name").getAsString());
            if(!note.get("customer_picture(for company)").isJsonNull())model.setImage(note.get("customer_picture(for company)").getAsString());
            if(!note.get("entertain").isJsonNull())model.setEntertain(note.get("entertain").getAsString());
            if(!note.get("customer_msg").isJsonNull())model.setMessage(note.get("customer_msg").getAsString());
            companylist.add(model);
        }
        companyAdapter = new Adapter_Notification_Company(companylist,this, sharedPreferences.getString("id","0"));
        recyclerView.setAdapter(companyAdapter);
    }
    private void loadcustomernotifications(JsonArray notifications)
    {
        ArrayList<Model_Notification_Company> list = new ArrayList<>();
        Model_Notification_Company model;
        for(int i = 0; i < notifications.size(); i++)
        {
            JsonObject note = notifications.get(i).getAsJsonObject();
            model = new Model_Notification_Company();
            if(!note.get("id").isJsonNull())model.setNotificaationid(note.get("id").getAsString());
            if(!note.get("company_id").isJsonNull())model.setCompanyid(note.get("company_id").getAsString());
            if(!note.get("company_name").isJsonNull())model.setCompanyname(note.get("company_name").getAsString());
            if(!note.get("customer_id").isJsonNull())model.setCustomerid(note.get("customer_id").getAsString());
            if(!note.get("customer_name").isJsonNull())model.setCustomername(note.get("customer_name").getAsString());
            if(!note.get("company_picture(for customer)").isJsonNull())model.setImage(note.get("company_picture(for customer)").getAsString());
            if(!note.get("entertain").isJsonNull(   ))model.setEntertain(note.get("entertain").getAsString());
            if(!note.get("company_msg").isJsonNull())model.setMessage(note.get("company_msg").getAsString());
            list.add(model);
        }
        recyclerView.setAdapter(new Adapter_Notification_Customer(list));
    }

    private boolean iscompany()
    {
        return sharedPreferences.getBoolean("startas",false);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(),this);
    }

    @Override
    public void acceptOffer(final int position)
    {
        final Dialog dialog = new Dialog(getContext(),android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_accept_offer);

//        dialogimgSelected = (ImageView)dialog.findViewById(R.id.dialog_acceptoffer_imgselected);
//        dialogTakePhotoLayout = (ViewGroup)dialog.findViewById(R.id.dialog_acceptoffer_takephotolayout);
//        TextView txttitle = (TextView)dialog.findViewById(R.id.dialog_acceptoffer_txttitle);
//        txttitle.setText("Accept Offer");
//        ImageButton btnclose = (ImageButton)dialog.findViewById(R.id.dialog_acceptoffer_btnclose);
//        final TextView btnaddphoto = (TextView) dialog.findViewById(R.id.dialog_acceptoffer_txtaddphoto);
//        btnaddphoto.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view)
//            {
//                if(checkpermission())
//                {
//                    btnaddphoto.setVisibility(View.GONE);
//                    dialogTakePhotoLayout.setVisibility(View.VISIBLE);
//                }
//            }
//        });
//        ImageButton btncamera = (ImageButton)dialog.findViewById(R.id.dialog_acceptoffer_btncamera);
//        btncamera.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                EasyImage.openCamera(Fragment_Notification.this,101);
//            }
//        });
//        ImageButton btngellry = (ImageButton)dialog.findViewById(R.id.dialog_acceptoffer_btngellry);
//        btngellry.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                EasyImage.openGallery(Fragment_Notification.this,101);
//            }
//        });
//
//        btnclose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.dismiss();
//            }
//        });
        final EditText txtmessage = (EditText)dialog.findViewById(R.id.etEmailMessage);
        Button btnsend = (Button)dialog.findViewById(R.id.dialog_acceptoffer_btnsend);
        btnsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                laodingdialog = new SpotsDialog(getContext(), R.style.dialog_style);
                laodingdialog.show();
                acceptOfferNetworkCall(position,txtmessage.getText().toString(), dialog);

            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }

    private void acceptOfferNetworkCall(final int position, String message, final Dialog dialog)
    {
        if(selectedImage == null)
        {
            Ion.with(getContext())
                    .load("http://www.superpvcworks.com/www.arzsaar.com/api/notification_accept.php")
                    .setMultipartParameter("notification_id",companylist.get(position).getNotificaationid())
                    .setMultipartParameter("customer_id",companylist.get(position).getCustomerid())
                    .setMultipartParameter("company_id",companylist.get(position).getCompanyid())
                    .setMultipartParameter("msg",message)
                    .asString().setCallback(new FutureCallback<String>() {
                @Override
                public void onCompleted(Exception e, String result)
                {
                    laodingdialog.dismiss();
                    if(e == null)
                    {
                        if(result.contains("\"success\":1"))
                        {
                            companylist.remove(position);
                            companyAdapter.notifyDataSetChanged();
                            dialog.dismiss();
                            String message = getResources().getString(R.string.successmessage);
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            String message = getResources().getString(R.string.somethingwrong);
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        String message = getResources().getString(R.string.connectionerror);
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
        else
        {
            Ion.with(getContext())
                    .load("http://www.superpvcworks.com/www.arzsaar.com/api/notification_accept.php")
                    .setMultipartParameter("notification_id",companylist.get(position).getNotificaationid())
                    .setMultipartParameter("customer_id",companylist.get(position).getCustomerid())
                    .setMultipartParameter("company_id",companylist.get(position).getCompanyid())
                    .setMultipartParameter("msg",message)
                    .setMultipartFile("image",selectedImage)
                    .asString().setCallback(new FutureCallback<String>() {
                @Override
                public void onCompleted(Exception e, String result)
                {
                    laodingdialog.dismiss();
                    if(e == null)
                    {
                        if(result.contains("\"success\":1"))
                        {
                            companylist.remove(position);
                            companyAdapter.notifyDataSetChanged();
                            dialog.dismiss();
                            String message = getResources().getString(R.string.successmessage);
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            String message = getResources().getString(R.string.somethingwrong);
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        String message = getResources().getString(R.string.connectionerror);
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }
//    @Override
//    public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
//
//    }
//
//    @Override
//    public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type)
//    {
//        dialogimgSelected.setVisibility(View.VISIBLE);
//        dialogTakePhotoLayout.setVisibility(View.GONE);
//        Bitmap myBitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
//        dialogimgSelected.setImageBitmap(myBitmap);
//        String photo = compressImage(String.valueOf(imageFile));
//        selectedImage = new File(photo);
//    }
//
//    @Override
//    public void onCanceled(EasyImage.ImageSource source, int type) {
//
//    }
//    private boolean checkpermission()
//    {
//        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
//        {
//            return true;
//        }
//        else
//        {
//            requespermission();
//            return false;
//        }
//    }

    private void requespermission()
    {
        ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},101);
    }

    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/offerprice");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getActivity().getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }
}
