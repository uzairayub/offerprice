package com.uzair.offer.price.CompanyFragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.Util;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.uzair.offer.price.Adapters.AdapterAllMaterials;
import com.uzair.offer.price.Models.ModelAllMaterial;
import com.uzair.offer.price.R;
import com.uzair.offer.price.Utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FragmentAllMaterials extends Fragment implements SwipeRefreshLayout.OnRefreshListener, AdapterAllMaterials.SelectionCallBack, View.OnClickListener {

    private ImageView imgerror;
    private TextView txterror;
    private View view;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout refresher;
    private SharedPreferences sharedPreferences;
    private ArrayList<ModelAllMaterial> list;
    private int selectedLang;
    private Button btnAdd;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_all_materials,container,false);
        initial();
        return view;
    }

    private void initial()
    {
        list = new ArrayList<>();
        btnAdd = view.findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);
        imgerror = (ImageView)view.findViewById(R.id.fragment_mylist_img_error);
        txterror = (TextView) view.findViewById(R.id.fragment_mylist_txt_error);

        sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        if(sharedPreferences.getString("lang","ar").equalsIgnoreCase("en"))
        {
            selectedLang = 1;
        }
        else
        {
            selectedLang = 2;
        }
        refresher = (SwipeRefreshLayout)view.findViewById(R.id.fragment_mylist_refresher);
        refresher.setOnRefreshListener(this);
        recyclerView = (RecyclerView)view.findViewById(R.id.fragment_mylist_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        getAllMaterialList();
    }

    @Override
    public void onRefresh()
    {
        if(list != null)
        {
            list.clear();
        }
        getAllMaterialList();
    }

    private void getAllMaterialList()
    {
        refresher.setRefreshing(true);
        Ion.with(getContext())
                .load(Constants.BASE_URL +"show_allMaterial.php")
                .setMultipartParameter("comp_id",sharedPreferences.getString("id","0"))
                .setMultipartParameter("lang",String.valueOf(selectedLang))
                .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                refresher.setRefreshing(false);
                if(e == null)
                {
                    JsonArray data = result.get("data").getAsJsonArray();
                    if(result.get("staus").getAsString().equals("1") && data.size() > 0)
                    {
                        showresults(data);
                    }
                    else
                    {
                        String message = getResources().getString(R.string.nothing_show);
                        showerror(message);
                    }
                }
                else
                {
                    String message = getResources().getString(R.string.connectionerror);
                    showerror(message);
                }
            }
        });
    }

    private void showresults(JsonArray result)
    {
        ModelAllMaterial model;
        for (int i = 0; i < result.size(); i++)
        {
            JsonObject material = result.get(i).getAsJsonObject();
            model = new ModelAllMaterial();
            model.setMaterialName(material.get("material_name").getAsString());
            model.setId(material.get("material_id").getAsString());
            model.setStatus(material.get("status").getAsBoolean());
            model.setSelected(false);
            list.add(model);
        }
        AdapterAllMaterials adapter = new AdapterAllMaterials(list,this);
        recyclerView.setAdapter(adapter);
        refresher.setVisibility(View.VISIBLE);
        imgerror.setVisibility(View.GONE);
        txterror.setVisibility(View.GONE);
    }

    private void showerror(String error)
    {
        btnAdd.setVisibility(View.GONE);
        refresher.setVisibility(View.GONE);
        imgerror.setVisibility(View.VISIBLE);
        txterror.setVisibility(View.VISIBLE);
        txterror.setText(error);
    }

    @Override
    public void onMaterialSelected(int position)
    {
        if(getSelectedMaterials().length() > 0)
        {
            btnAdd.setVisibility(View.VISIBLE);
        }
        else
        {
            btnAdd.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view)
    {
        if(view.getId() == btnAdd.getId())
        {
            btnAdd.setVisibility(View.GONE);
            uploadSelectedMaterial(getSelectedMaterials());
            Toast.makeText(getContext(),getResources().getString(R.string.pleasewait),Toast.LENGTH_LONG).show();
        }
    }

    private JSONArray getSelectedMaterials()
    {
        JSONArray selectedMaterials = null;
        try
        {
            selectedMaterials = new JSONArray();
            for(int i = 0; i < list.size(); i++)
            {
                if(list.get(i).isSelected())
                {
                    JSONObject object = new JSONObject();
                    int id = Integer.parseInt(list.get(i).getId());
                    object.put("mat_id",id);
                    selectedMaterials.put(object);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return selectedMaterials;

    }

    private void uploadSelectedMaterial(JSONArray selectedMaterials)
    {
        Ion.with(getContext())
                .load(Constants.BASE_URL+"add_multipleMaterial.php")
                .setBodyParameter("input",selectedMaterials.toString())
                .setBodyParameter("comp_id",sharedPreferences.getString("id","0"))
                .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result)
            {
                if(e == null)
                {
                    if(result.get("status").getAsString().equalsIgnoreCase("1"))
                    {

                        String message = getResources().getString(R.string.successmessage);
                        Toast.makeText(getContext(),message,Toast.LENGTH_LONG).show();
                        list.clear();
                        getAllMaterialList();
                    }
                    else
                    {
                        String message = getResources().getString(R.string.errormessage);
                        Toast.makeText(getContext(),message,Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    String message = getResources().getString(R.string.connectionerror);
                    Toast.makeText(getContext(),message,Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
