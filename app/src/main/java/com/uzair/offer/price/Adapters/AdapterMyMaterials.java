package com.uzair.offer.price.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.uzair.offer.price.Models.ModelAllMaterial;
import com.uzair.offer.price.R;

import java.util.ArrayList;

public class AdapterMyMaterials extends RecyclerView.Adapter<AdapterMyMaterials.MyViewHolder> {

    private Context context;
    private ArrayList<ModelAllMaterial> list;
    private SelectionCallBack selectionCallBack;

    public AdapterMyMaterials(ArrayList<ModelAllMaterial> list, SelectionCallBack selectionCallBack) {
        this.list = list;
        this.selectionCallBack = selectionCallBack;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_material, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        holder.tvMaterialName.setText(list.get(position).getMaterialName());
        holder.cbAdd.setChecked(list.get(position).isSelected());
        holder.cbAdd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b)
            {
                list.get(position).setSelected(b);
                selectionCallBack.onMaterialSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvMaterialName;
        CheckBox cbAdd;
        ImageView imgAdded;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.setIsRecyclable(false);
            tvMaterialName = itemView.findViewById(R.id.tvMaterialName);
            cbAdd = itemView.findViewById(R.id.cbAdd);
            imgAdded = itemView.findViewById(R.id.imgAdded);
            imgAdded.setVisibility(View.GONE);
        }
    }


    public interface SelectionCallBack
    {
        void onMaterialSelected(int position);
    }
}
