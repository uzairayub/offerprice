package com.uzair.offer.price.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.uzair.offer.price.R;

public class FullScreenImage extends AppCompatActivity {

    private ImageView imageView;
    private ImageButton btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_full_screen_image);

        initial();
    }

    private void initial()
    {
        imageView = findViewById(R.id.imageView);
        String imageUrl = getIntent().getExtras().getString("image");
        Picasso.get().load(imageUrl).placeholder(R.drawable.ic_picture_placeholder).error(R.drawable.ic_picture_placeholder).into(imageView);
        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
