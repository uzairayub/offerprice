package com.uzair.offer.price.CompanyFragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.uzair.offer.price.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import dmax.dialog.SpotsDialog;
//import pl.aprilapps.easyphotopicker.EasyImage;

/**
 * Created by uzairayub on 1/31/18.
 */

public class Fragment_AddNew extends Fragment implements FutureCallback<String>, View.OnClickListener{

    private View view;
    private ImageView imageView;
    private Button btnupload;
    private EditText txtname, txtweight, txtcolor, txtnumber, txtunit, txtconsumption, txtinch, txtthikness, txttype, txtheight, txtlenght, txtdiameter, txtsize;
    private SharedPreferences sharedPreferences;
    private String id;
    private File file;
    private AlertDialog dialog;
//    private
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_addnew,container,false);

        initial();
        return view;
    }

    private void initial()
    {
        sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("id","0");

        imageView = (ImageView)view.findViewById(R.id.addnew_image);
        imageView.setOnClickListener(this);

        txtname = (EditText)view.findViewById(R.id.addnew_name);
        txtweight = (EditText)view.findViewById(R.id.addnew_weight);
        txtcolor = (EditText)view.findViewById(R.id.addnew_color);
        txtnumber = (EditText)view.findViewById(R.id.addnew_number);
        txtunit = (EditText)view.findViewById(R.id.addnew_unit);
        txtconsumption = (EditText)view.findViewById(R.id.addnew_consumption);
        txtinch = (EditText)view.findViewById(R.id.addnew_inch);
        txtthikness = (EditText)view.findViewById(R.id.addnew_thickness);
        txttype = (EditText)view.findViewById(R.id.addnew_type);
        txtheight = (EditText)view.findViewById(R.id.addnew_height);
        txtlenght = (EditText)view.findViewById(R.id.addnew_length);
        txtdiameter = (EditText)view.findViewById(R.id.addnew_diameter);
        txtsize = (EditText)view.findViewById(R.id.addnew_size);

        btnupload = (Button)view.findViewById(R.id.addnew_btnupload);
        btnupload.setOnClickListener(this);
    }
    private void upload_material()
    {
        dialog = new SpotsDialog(getContext(), R.style.dialog_style);
        dialog.show();
        Ion.with(getContext())
                .load("http://www.arzsaar.com/addmaterials.php")
                .setMultipartParameter("company_id",id)
                .setMultipartParameter("material_name",txtname.getText().toString())
                .setMultipartParameter("weight",txtweight.getText().toString())
                .setMultipartParameter("color",txtcolor.getText().toString())
                .setMultipartParameter("number",txtnumber.getText().toString())
                .setMultipartParameter("unit",txtunit.getText().toString())
                .setMultipartParameter("consumption",txtconsumption.getText().toString())
                .setMultipartParameter("inch",txtinch.getText().toString())
                .setMultipartParameter("thickness",txtthikness.getText().toString())
                .setMultipartParameter("type",txttype.getText().toString())
                .setMultipartParameter("height",txtheight.getText().toString())
                .setMultipartParameter("length",txtlenght.getText().toString())
                .setMultipartParameter("diameter",txtdiameter.getText().toString())
                .setMultipartParameter("size",txtsize.getText().toString())
                .setMultipartFile("picture",file)
                .asString().setCallback(this);
    }

    @Override
    public void onCompleted(Exception e, String result)
    {
        dialog.dismiss();
        if(e == null)
        {
            String message = getResources().getString(R.string.successmessage);
            Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            txtname.setText("");
            txtweight.setText("");
            txtcolor.setText("");
            txtnumber.setText("");
            txtunit.setText("");
            txtconsumption.setText("");
            txtinch.setText("");
            txtthikness.setText("");
            txttype.setText("");
            txtheight.setText("");
            txtlenght.setText("");
            txtdiameter.setText("");
            txtsize.setText("");
            imageView.setImageResource(R.drawable.ic_addphoto);
        }
        else
        {
            String message = getResources().getString(R.string.somethingwrong);
            Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
        }
    }
    @Override
    public void onClick(View view)
    {
        if(view.getId() == imageView.getId())
        {
            if(checkpermission())
            {
//                showdialouge();
            }
        }
        else if (view.getId() == btnupload.getId())
        {
            if(checkvalidation())
            {
                upload_material();
            }
        }
    }

    private boolean checkvalidation()
    {
        if(file != null)
        {
            if(!txtname.getText().toString().isEmpty())
            {
                return true;
            }
            else
            {
                txtname.setError("Please enter product Name");
                return false;
            }
        }
        else
        {
            String message = getResources().getString(R.string.photo);
            Toast.makeText(getContext(),message,Toast.LENGTH_LONG).show();
            return false;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(),this);
    }

    private boolean checkpermission()
    {
        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
        {
            return true;
        }
        else
        {
            requespermission();
            return false;
        }
    }

    private void requespermission()
    {
        ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},101);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 101)
        {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
            {
                imageView.performClick();
            }
            else
            {
                requespermission();
            }
        }
    }

//    @Override
//    public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type)
//    {
//
//    }
//    @Override
//    public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type)
//    {
//        if(imageFile != null)
//        {
//            Bitmap myBitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
//            imageView.setImageBitmap(myBitmap);
//            String photo = compressImage(String.valueOf(imageFile));
//            file = new File(photo);
//        }
//
//    }
//    @Override
//    public void onCanceled(EasyImage.ImageSource source, int type)
//    {
//
//    }

//    void showdialouge ()
//    {
//        final Dialog dialog = new Dialog(getContext(),android.R.style.Theme_Translucent_NoTitleBar);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_imagepicker);
//
//        ImageView btncamera = (ImageView)dialog.findViewById(R.id.dialog_imagepicker_camera);
//        btncamera.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view)
//            {
////                EasyImage.openCamera(Fragment_AddNew.this,101);
//                dialog.dismiss();
//            }
//        });
//        ImageView btngallry = (ImageView)dialog.findViewById(R.id.dialog_imagepicker_gallry);
//        btngallry.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view)
//            {
////                EasyImage.openGallery(Fragment_AddNew.this,101);
//                dialog.dismiss();
//            }
//        });
//
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(dialog.getWindow().getAttributes());
//        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        lp.gravity = Gravity.CENTER;
//        dialog.getWindow().setAttributes(lp);
//        dialog.show();
//    }


    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public String getFilename()
    {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/offerprice");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getActivity().getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }
}
