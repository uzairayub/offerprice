package com.uzair.offer.price.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.uzair.offer.price.Models.ModelAllMaterial;
import com.uzair.offer.price.R;

import java.util.ArrayList;

public class AdapterOtherCompanyMaterials extends RecyclerView.Adapter<AdapterOtherCompanyMaterials.MyViewHolder> {

    private ArrayList<ModelAllMaterial> list;
    private Context context;

    public AdapterOtherCompanyMaterials(ArrayList<ModelAllMaterial> list) {
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_material, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
        holder.tvMaterialName.setText(list.get(position).getMaterialName());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvMaterialName;
        CheckBox cbAdd;
        ImageView imgAdded;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvMaterialName = itemView.findViewById(R.id.tvMaterialName);
            cbAdd = itemView.findViewById(R.id.cbAdd);
            cbAdd.setVisibility(View.GONE);
            imgAdded = itemView.findViewById(R.id.imgAdded);
            imgAdded.setVisibility(View.GONE);
        }
    }
}
