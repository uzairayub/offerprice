package com.uzair.offer.price.Models.Inbox;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Customer implements Serializable {

	@SerializedName("customer_password")
	private String customerPassword;

	@SerializedName("customer_address")
	private String customerAddress;

	@SerializedName("location_name")
	private String locationName;

	@SerializedName("customer_about")
	private String customerAbout;

	@SerializedName("customer_joindate")
	private String customerJoindate;

	@SerializedName("customer_email")
	private String customerEmail;

	@SerializedName("customer_picture")
	private String customerPicture;

	@SerializedName("customer_name")
	private String customerName;

	@SerializedName("customer_id")
	private int customerId;

	@SerializedName("location_id")
	private String locationId;

	@SerializedName("token")
	private String token;

	public void setCustomerPassword(String customerPassword){
		this.customerPassword = customerPassword;
	}

	public String getCustomerPassword(){
		return customerPassword;
	}

	public void setCustomerAddress(String customerAddress){
		this.customerAddress = customerAddress;
	}

	public String getCustomerAddress(){
		return customerAddress;
	}

	public void setLocationName(String locationName){
		this.locationName = locationName;
	}

	public String getLocationName(){
		return locationName;
	}

	public void setCustomerAbout(String customerAbout){
		this.customerAbout = customerAbout;
	}

	public String getCustomerAbout(){
		return customerAbout;
	}

	public void setCustomerJoindate(String customerJoindate){
		this.customerJoindate = customerJoindate;
	}

	public String getCustomerJoindate(){
		return customerJoindate;
	}

	public void setCustomerEmail(String customerEmail){
		this.customerEmail = customerEmail;
	}

	public String getCustomerEmail(){
		return customerEmail;
	}

	public void setCustomerPicture(String customerPicture){
		this.customerPicture = customerPicture;
	}

	public String getCustomerPicture(){
		return customerPicture;
	}

	public void setCustomerName(String customerName){
		this.customerName = customerName;
	}

	public String getCustomerName(){
		return customerName;
	}

	public void setCustomerId(int customerId){
		this.customerId = customerId;
	}

	public int getCustomerId(){
		return customerId;
	}

	public void setLocationId(String locationId){
		this.locationId = locationId;
	}

	public String getLocationId(){
		return locationId;
	}

	public void setToken(String token){
		this.token = token;
	}

	public String getToken(){
		return token;
	}

	@Override
 	public String toString(){
		return 
			"Customer{" + 
			"customer_password = '" + customerPassword + '\'' + 
			",customer_address = '" + customerAddress + '\'' + 
			",location_name = '" + locationName + '\'' + 
			",customer_about = '" + customerAbout + '\'' + 
			",customer_joindate = '" + customerJoindate + '\'' + 
			",customer_email = '" + customerEmail + '\'' + 
			",customer_picture = '" + customerPicture + '\'' + 
			",customer_name = '" + customerName + '\'' + 
			",customer_id = '" + customerId + '\'' + 
			",location_id = '" + locationId + '\'' + 
			",token = '" + token + '\'' + 
			"}";
		}
}