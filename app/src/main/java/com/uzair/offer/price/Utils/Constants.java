package com.uzair.offer.price.Utils;

public interface Constants {

    String BASE_URL_NEW = "http://www.arzsaar.com/offerprice/api/";
    String BASE_URL = "http://arzsaar.com/offer_price/";
    String BASE_URL_old = "http://arzsaar.com/";
    String CHAT_MEDIA_URL = "http://www.arzsaar.com/offerprice/uploads/media";

    String FOREGROUND = "foreground";
}
