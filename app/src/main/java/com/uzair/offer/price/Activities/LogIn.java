package com.uzair.offer.price.Activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.uzair.offer.price.Models.Inbox.InboxResponse;
import com.uzair.offer.price.R;
import com.uzair.offer.price.Utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import dmax.dialog.SpotsDialog;

public class LogIn extends AppCompatActivity implements View.OnClickListener {

    private CheckBox cbKeepLogin;
    private TextView txtsignup, txttitle, txtforgotpassword;
    private EditText txtemail, txtpassword;
    private Button btnlogin;
    private boolean startas;
    private SharedPreferences sharedPreferences;
    private String token;
    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        createToken();
        initial();

    }

    private void createToken()
    {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( LogIn.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                token = instanceIdResult.getToken();
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void initial()
    {
        sharedPreferences = getSharedPreferences("user",MODE_PRIVATE);
        startas = (getIntent().getExtras()).getBoolean("startas");
        txttitle = (TextView)findViewById(R.id.login_txttitle);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/ElectroShackle.otf");
        txttitle.setTypeface(typeface);
        txtsignup = (TextView)findViewById(R.id.login_txtsignup);
        txtsignup.setOnClickListener(this);
        txtforgotpassword = (TextView)findViewById(R.id.login_txt_forgetpassword);
        txtforgotpassword.setOnClickListener(this);
        btnlogin = (Button)findViewById(R.id.login_btnlogin);
        btnlogin.setOnClickListener(this);
        cbKeepLogin = findViewById(R.id.cbKeepLogin);

        txtemail = (EditText)findViewById(R.id.login_txt_email);
        txtpassword = (EditText)findViewById(R.id.login_txt_password);
    }

    @Override
    public void onClick(View view)
    {
        if(view.getId() == txtsignup.getId())
        {
            boolean startas = getIntent().getExtras().getBoolean("startas");
            Intent intent = new Intent(LogIn.this,SignUp.class);
            intent.putExtra("startas",startas);
            startActivity(intent);
        }
        else if(view.getId() == btnlogin.getId())
        {
            if(check_validation())
            {
                singIn();
            }
        }
        else if(view.getId() == txtforgotpassword.getId())
        {
            startActivity(new Intent(LogIn.this,ForgetPassword.class));
        }

    }

    private  void singIn()
    {

        dialog = new SpotsDialog(LogIn.this, R.style.dialog_style);
        dialog.show();


        Ion.with(LogIn.this).load(getUrl())
                .setBodyParameter("email",txtemail.getText().toString())
                .setBodyParameter("password",txtpassword.getText().toString())
                .setBodyParameter("token", token)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result)
                    {
                        if(e != null)
                        {
                            dialog.dismiss();
                            return;
                        }
                        if(result.get("status").getAsString().equalsIgnoreCase("success"))
                        {
                            String message = getResources().getString(R.string.successmessage);
                            Toast.makeText(LogIn.this,message,Toast.LENGTH_LONG).show();
                            if(startas)
                            {
                                getcompany(result);
                            }
                            else
                            {
                                getcustomer(result);
                            }
                        }
                        else
                        {
                            dialog.dismiss();
                            String message = getResources().getString(R.string.somethingwrong);
                            Toast.makeText(LogIn.this,message,Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void getcustomer(JsonObject response)
    {
        JsonObject data = response.get("data").getAsJsonObject();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("id",data.get("customer_id").getAsString());
        editor.putString("name",data.get("customer_name").getAsString());
        editor.putString("email",data.get("customer_email").getAsString());
        editor.putString("about",data.get("customer_about").getAsString());
        editor.putString("photo",data.get("customer_picture").getAsString());
        editor.putString("address",data.get("customer_address").getAsString());
        editor.putString("location",data.get("location_name").getAsString());
        editor.putBoolean("keepLogin",cbKeepLogin.isChecked());
        editor.commit();
        getCustomerInbox();
    }

    private void getcompany(JsonObject response)
    {
        JsonObject data = response.get("data").getAsJsonObject();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("id",data.get("company_id").getAsString());
        editor.putString("name",data.get("company_name").getAsString());
        editor.putString("email",data.get("company_email").getAsString());
        editor.putString("about",data.get("company_about").getAsString());
        editor.putString("photo",data.get("company_picture").getAsString());
        editor.putString("address",data.get("company_address").getAsString());
        editor.putString("location",data.get("location_name").getAsString());
        editor.putBoolean("keepLogin",cbKeepLogin.isChecked());
        editor.commit();
        getCompanyInbox();
    }
    private void startScreen()
    {
        Intent intent = new Intent(LogIn.this,MainActivity.class);
        intent.putExtra("notification",false);
        startActivity(intent);
    }
    private String getUrl()
    {
        if(startas)
        {
            return Constants.BASE_URL_old+"company_signin.php";
        }
        else
        {
            return Constants.BASE_URL_old+"customer_signin.php";
        }
    }

    private boolean check_validation()
    {
        if (!txtemail.getText().toString().isEmpty() && txtemail.getText().toString().contains("@")) {
            if (txtpassword.getText().toString().length() > 6)
            {
                return true;
            }
            else
            {
                txtpassword.setError(getResources().getString(R.string.shot_password));
                return false;
            }

        } else {
            txtemail.setError(getResources().getString(R.string.valid_email));
            return false;
        }
    }



    private void getCompanyInbox() {
        String id = sharedPreferences.getString("id", "0");
        Ion.with(LogIn.this)
                .load(Constants.BASE_URL_NEW + "company-threads?company_id=" + id)
                .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                dialog.dismiss();
                if (e == null) {
                    if (result.get("status").getAsBoolean()) {
                        InboxResponse inboxResponse = new Gson().fromJson(result, InboxResponse.class);
                        int count = 0;
                        for (int i = 0; i < inboxResponse.getData().size(); i++) {
                            if (inboxResponse.getData().get(i).getUnseenByCompanyCount() > 0) {
                                count++;
                            }
                        }
                        Intent intent = new Intent(LogIn.this, MainActivity.class);
                        intent.putExtra("notification", false);
                        intent.putExtra("messageCount", count);
                        startActivity(intent);
                        finish();
                        return;
                    }
                    Intent intent = new Intent(LogIn.this, MainActivity.class);
                    intent.putExtra("notification", false);
                    intent.putExtra("messageCount", 0);
                    startActivity(intent);
                    finish();
                    return;
                }
                Intent intent = new Intent(LogIn.this, MainActivity.class);
                intent.putExtra("notification", false);
                intent.putExtra("messageCount", 0);
                startActivity(intent);
                finish();
            }
        });

    }

    private void getCustomerInbox() {
        String id = sharedPreferences.getString("id", "0");
        Ion.with(LogIn.this)
                .load(Constants.BASE_URL_NEW+"customer-threads?customer_id="+id)
                .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                dialog.dismiss();
                if (e == null) {
                    if (result.get("status").getAsBoolean()) {
                        InboxResponse inboxResponse = new Gson().fromJson(result, InboxResponse.class);
                        int count = 0;
                        for (int i = 0; i < inboxResponse.getData().size(); i++) {
                            if (inboxResponse.getData().get(i).getUnseenByCustomerCount() > 0) {
                                count++;
                            }
                        }
                        Intent intent = new Intent(LogIn.this, MainActivity.class);
                        intent.putExtra("notification", false);
                        intent.putExtra("messageCount", count);
                        startActivity(intent);
                        finish();
                        return;
                    }
                    Intent intent = new Intent(LogIn.this, MainActivity.class);
                    intent.putExtra("notification", false);
                    intent.putExtra("messageCount", 0);
                    startActivity(intent);
                    finish();
                    return;
                }
                Intent intent = new Intent(LogIn.this, MainActivity.class);
                intent.putExtra("notification", false);
                intent.putExtra("messageCount", 0);
                startActivity(intent);
                finish();
                return;
            }
        });
    }
}
