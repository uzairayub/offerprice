package com.uzair.offer.price.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.uzair.offer.price.Models.Inbox.InboxItem;
import com.uzair.offer.price.R;
import com.uzair.offer.price.Utils.Constants;
import com.uzair.offer.price.Utils.Util;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterCompanyInbox extends RecyclerView.Adapter<AdapterCompanyInbox.MyViewHolder> {

    private ArrayList<InboxItem> list;
    private Context context;
    private CompanyInboxCallback callback;
    private String lang;

    public AdapterCompanyInbox(ArrayList<InboxItem> list, CompanyInboxCallback callback, String lang) {
        this.list = list;
        this.callback = callback;
        this.lang = lang;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        this.context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_inbox, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position)
    {
        String url = Constants.BASE_URL_old +"customerpictures/"+list.get(position).getCustomer().getCustomerPicture();
        Picasso.get().load(url).placeholder(R.drawable.logo).into(holder.ivImage);

        holder.tvName.setText(list.get(position).getCustomer().getCustomerName());
        if(lang.equalsIgnoreCase("en"))
        {
            holder.tvMaterialName.setText(list.get(position).getMaterial().getNameEng());
        }
        else
        {
            holder.tvMaterialName.setText(list.get(position).getMaterial().getNameArabic());
        }
        holder.tvLastMessage.setText(list.get(position).getLastMessage().getText());
        holder.tvTime.setText(Util.parseDate(list.get(position).getCreatedAt().getDate(), context));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onItemClicked(position);
            }
        });
        if(list.get(position).getUnseenByCompanyCount() > 0)
        {
            holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimaryTransparent));
        }
        else
        {
            holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView ivImage;
        TextView tvName, tvMaterialName, tvLastMessage, tvTime;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ivImage = itemView.findViewById(R.id.ivImage);
            tvName = itemView.findViewById(R.id.tvName);
            tvMaterialName = itemView.findViewById(R.id.tvMaterialName);
            tvLastMessage = itemView.findViewById(R.id.tvLastMessage);
            tvTime = itemView.findViewById(R.id.tvTime);
        }
    }

    public interface CompanyInboxCallback
    {
        void onItemClicked(int position);
    }
}
