package com.uzair.offer.price.Models.Inbox;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class InboxResponse{

	@SerializedName("data")
	private List<InboxItem> data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private boolean status;

	public List<InboxItem> getData() {
		return data;
	}

	public void setData(List<InboxItem> data) {
		this.data = data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"InboxResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}