package com.uzair.offer.price.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.uzair.offer.price.Adapters.AdapterAllMaterials;
import com.uzair.offer.price.Adapters.AdapterOtherCompanyMaterials;
import com.uzair.offer.price.Models.ModelAllMaterial;
import com.uzair.offer.price.Models.Model_All_Material_List;
import com.uzair.offer.price.R;
import com.uzair.offer.price.Utils.Constants;

import java.util.ArrayList;

public class Material_List extends AppCompatActivity implements  SwipeRefreshLayout.OnRefreshListener, FutureCallback<JsonObject>,AdapterAllMaterials.SelectionCallBack {

    private RecyclerView recyclerView;
    private SwipeRefreshLayout refresher;
    private ImageView imgnothing;
    private TextView txtnothing;
    private int selectedLang;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material_list);
        initial();
    }

    private void initial()
    {
        SharedPreferences sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        if(sharedPreferences.getString("lang","ar").equalsIgnoreCase("en"))
        {
            selectedLang = 1;
        }
        else
        {
            selectedLang = 2;
        }
        Toolbar toolbar = (Toolbar)findViewById(R.id.materialist_toolbar);
        toolbar.setTitle(getIntent().getExtras().getString("name"));
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        imgnothing = (ImageView)findViewById(R.id.materiallist_img_nothing);
        txtnothing = (TextView)findViewById(R.id.materiallist_txt_nothing);

        refresher = (SwipeRefreshLayout)findViewById(R.id.materiallist_refresher);
        refresher.setOnRefreshListener(this);
        recyclerView = (RecyclerView)findViewById(R.id.materiallist_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(Material_List.this));
        getcompanymaterial(getIntent().getExtras().getString("id"));

    }

    private void getcompanymaterial(String id)
    {
        refresher.setRefreshing(true);
        Ion.with(Material_List.this)
                .load(Constants.BASE_URL +"show_material.php")
                .setMultipartParameter("comp_id",id)
                .setMultipartParameter("lang",String.valueOf(selectedLang))
                .asJsonObject().setCallback(this);
    }


    @Override
    public void onRefresh()
    {
        getcompanymaterial(getIntent().getExtras().getString("id"));
    }

    @Override
    public void onCompleted(Exception e, JsonObject result)
    {
        refresher.setRefreshing(false);
        if(e == null)
        {
            if(result.get("ststus").getAsString().equalsIgnoreCase("1"))
            {
                ArrayList<ModelAllMaterial> list = new ArrayList<>();
                ModelAllMaterial model;
                JsonArray data = result.get("data").getAsJsonArray();
                for(int i = 0; i < data.size(); i++)
                {
                    JsonObject material = data.get(i).getAsJsonObject();
                    model = new ModelAllMaterial();
                    model.setMaterialName(material.get("material_name").getAsString());
                    model.setId(material.get("material_id").getAsString());
                    list.add(model);
                }
                recyclerView.setAdapter(new AdapterOtherCompanyMaterials(list));
            }
            else
            {
                String message = getResources().getString(R.string.nothing_show);
                showerror(message);
            }
        }
        else
        {
            String message = getResources().getString(R.string.connectionerror);
            showerror(message);
        }
    }

    private void showerror(String error)
    {
        refresher.setVisibility(View.GONE);
        imgnothing.setVisibility(View.VISIBLE);
        txtnothing.setVisibility(View.VISIBLE);
        txtnothing.setText(error);
    }

    @Override
    public void onMaterialSelected(int position) {

    }
}
