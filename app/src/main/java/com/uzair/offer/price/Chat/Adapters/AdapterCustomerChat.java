package com.uzair.offer.price.Chat.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.uzair.offer.price.Models.Messages.ChattingItem;
import com.uzair.offer.price.R;
import com.uzair.offer.price.Utils.Util;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterCustomerChat extends RecyclerView.Adapter<AdapterCustomerChat.MyViewHolder> {

    private ArrayList<ChattingItem> list;
    private Context context;

    public AdapterCustomerChat(ArrayList<ChattingItem> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View view = null;
        if (viewType == 0)
        {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_dark, parent, false);
            return new MyViewHolder(view);
        }
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_lite, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemViewType(int position)
    {
        if (list.get(position).getSentBy().equalsIgnoreCase("customer"))
        {
            return 1;
        }
        return 0;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position)
    {
        if(getItemViewType(position) == 0)
        {
//            Populate user photo
        }
        holder.tvMessage.setText(list.get(position).getText());
        if(list.get(position).getCreatedAt() != null)
        {
            holder.tvMessageTime.setText(Util.parseDate(list.get(position).getCreatedAt().getDate(), context));
            if(list.get(position).getFiles().size() > 0)
            {
                holder.llMessage.getLayoutParams().width= ViewGroup.LayoutParams.MATCH_PARENT;
                holder.rvFiles.setVisibility(View.VISIBLE);
                holder.rvFiles.setAdapter(new AdapterFiles(list.get(position).getFiles()));
            }
            else
            {
                holder.rvFiles.setVisibility(View.GONE);
                holder.llMessage.getLayoutParams().width= ViewGroup.LayoutParams.WRAP_CONTENT;
            }
        }
        else
        {
            holder.tvMessageTime.setText("Sending...");
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout llMessage;
        ImageView ivUserImage;
        TextView tvMessage, tvMessageTime;
        RecyclerView rvFiles;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            llMessage = itemView.findViewById(R.id.llMessage);
            ivUserImage = itemView.findViewById(R.id.ivUserImage);
            tvMessage = itemView.findViewById(R.id.tvMessage);
            tvMessageTime = itemView.findViewById(R.id.tvMessageTime);
            rvFiles = itemView.findViewById(R.id.rvFiles);
            SnapHelper snapHelper = new PagerSnapHelper();
            snapHelper.attachToRecyclerView(rvFiles);
            rvFiles.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        }
    }
}
