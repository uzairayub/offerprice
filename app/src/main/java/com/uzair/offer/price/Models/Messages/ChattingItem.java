package com.uzair.offer.price.Models.Messages;

import com.google.gson.annotations.SerializedName;
import com.uzair.offer.price.Models.Inbox.CreatedAt;
import com.uzair.offer.price.Models.Inbox.FilesItem;
import com.uzair.offer.price.Models.Inbox.UpdatedAt;

import java.io.Serializable;
import java.util.List;

public class ChattingItem implements Serializable {

	@SerializedName("sent_by")
	private String sentBy;

	@SerializedName("updated_at")
	private UpdatedAt updatedAt;

	@SerializedName("files")
	private List<FilesItem> files;

	@SerializedName("created_at")
	private CreatedAt createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("thread_id")
	private int threadId;

	@SerializedName("text")
	private String text;

	public int getThreadId() {
		return threadId;
	}

	public void setThreadId(int threadId) {
		this.threadId = threadId;
	}

	public String getSentBy() {
		return sentBy;
	}

	public void setSentBy(String sentBy) {
		this.sentBy = sentBy;
	}

	public UpdatedAt getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(UpdatedAt updatedAt) {
		this.updatedAt = updatedAt;
	}

	public List<FilesItem> getFiles() {
		return files;
	}

	public void setFiles(List<FilesItem> files) {
		this.files = files;
	}

	public CreatedAt getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(CreatedAt createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}