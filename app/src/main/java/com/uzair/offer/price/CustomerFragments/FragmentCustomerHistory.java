package com.uzair.offer.price.CustomerFragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.uzair.offer.price.Adapters.AdapterCustomerHistory;
import com.uzair.offer.price.Adapters.AdapterCustomerInbox;
import com.uzair.offer.price.Chat.CustomerChat;
import com.uzair.offer.price.Models.Inbox.InboxItem;
import com.uzair.offer.price.Models.Inbox.InboxResponse;
import com.uzair.offer.price.R;
import com.uzair.offer.price.Utils.Constants;

import java.util.ArrayList;

public class FragmentCustomerHistory extends Fragment implements SwipeRefreshLayout.OnRefreshListener, AdapterCustomerHistory.CustomerHistoryCallback{

    private View view;
    private ViewGroup llHistoryTitle;
    private ArrayList<InboxItem> list;
    private SwipeRefreshLayout refresher;
    private RecyclerView rvCompanyInbox;
    private AdapterCustomerHistory adapter;
    private SharedPreferences sharedPreferences;
    private String lang;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_company_inbox, container, false);
        initial();
        return view;
    }

    private void initial()
    {
        sharedPreferences = getActivity().getSharedPreferences("user",Context.MODE_PRIVATE);
        lang = sharedPreferences.getString("lang","ar");
        llHistoryTitle = view.findViewById(R.id.llHistoryTitle);
        llHistoryTitle.setVisibility(View.VISIBLE);
        refresher = view.findViewById(R.id.refresher);
        refresher.setOnRefreshListener(this);
        rvCompanyInbox = view.findViewById(R.id.rvCompanyInbox);
        rvCompanyInbox.setLayoutManager(new LinearLayoutManager(getContext()));
        list = new ArrayList<>();
        adapter = new AdapterCustomerHistory(list, this, lang);
        rvCompanyInbox.setAdapter(adapter);
        getCustomerInbox();
        refresher.setRefreshing(true);
    }


    private void getCustomerInbox()
    {
        String id = sharedPreferences.getString("id","0");
        Ion.with(getContext())
                .load(Constants.BASE_URL_NEW+"customer-threads?customer_id="+id)
                .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                refresher.setRefreshing(false);
                if(e == null)
                {
                    if(result.get("status").getAsBoolean())
                    {
                        InboxResponse inboxResponse = new Gson().fromJson(result, InboxResponse.class);
                        list.addAll(inboxResponse.getData());
                        adapter.notifyDataSetChanged();
                    }
                    return;
                }

                String message = getResources().getString(R.string.connectionerror);
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onRefresh()
    {
        list.clear();
        getCustomerInbox();
    }

    @Override
    public void onItemClicked(int position)
    {
        Intent intent = new Intent(getContext(), CustomerChat.class);
        intent.putExtra("item", list.get(position));
        startActivity(intent);
    }
}
