package com.uzair.offer.price.Models.LocationMaterial;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class MaterialsResponse{

	@SerializedName("data")
	private List<MaterialsItem> materials;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private boolean status;

	public void setMaterials(List<MaterialsItem> materials){
		this.materials = materials;
	}

	public List<MaterialsItem> getMaterials(){
		return materials;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"MaterialsResponse{" + 
			"materials = '" + materials + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}