package com.uzair.offer.price.Chat.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.uzair.offer.price.Activities.FullScreenImage;
import com.uzair.offer.price.Models.Inbox.FilesItem;
import com.uzair.offer.price.R;
import com.uzair.offer.price.Utils.Util;

import java.util.ArrayList;
import java.util.List;

public class AdapterFiles extends RecyclerView.Adapter<AdapterFiles.MyViewHolder> {

    private List<FilesItem> list;
    private Context context;

    public AdapterFiles(List<FilesItem> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        this.context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_file, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position)
    {
        if(list.get(position).getFilePath().contains("pdf"))
        {
            holder.imageView.setImageResource(R.drawable.ic_pdf);
        }
        else if(list.get(position).getFilePath().contains("docx"))
        {
            holder.imageView.setImageResource(R.drawable.ic_doc);
        }
        else
        {
            Picasso.get().load(list.get(position).getFilePath()).placeholder(R.drawable.ic_picture_placeholder).error(R.drawable.ic_picture_placeholder).into(holder.imageView, new Callback() {
                @Override
                public void onSuccess() {
                    Log.d("","");
                }

                @Override
                public void onError(Exception e) {
                    Log.d("","");
                }
            });
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(list.get(position).getFilePath().contains("pdf") || list.get(position).getFilePath().contains("docx"))
                {
                    Util.launchURL(context, list.get(position).getFilePath());
                }
                else
                {
                    context.startActivity(new Intent(context, FullScreenImage.class).putExtra("image", list.get(position).getFilePath()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
        }
    }
}
