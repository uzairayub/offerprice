package com.uzair.offer.price.OfferApprovel;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.util.IOUtils;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.async.http.body.FilePart;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.ion.Ion;
import com.uzair.offer.price.R;
import com.uzair.offer.price.Utils.Constants;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.ImagePickActivity;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.ImageFile;
import com.vincent.filepicker.filter.entity.NormalFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import dmax.dialog.SpotsDialog;

import static com.vincent.filepicker.activity.ImagePickActivity.IS_NEED_CAMERA;

public class CreateOffer extends AppCompatActivity implements View.OnClickListener {

    private EditText etMessage;
    private TextView tvFileOne, tvFileTwo, tvFileThree, tvFileFour, tvFileFive;
    private Button btnSend;
    private String message;
    private ArrayList<File> selectFiles;
    private SharedPreferences sharedPreferences;
    private int currentCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_offer);

        initial();
    }

    private void initial() {
        sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);

        selectFiles = new ArrayList<>();
        etMessage = findViewById(R.id.etMessage);
        tvFileOne = findViewById(R.id.tvFileOne);
        tvFileOne.setOnClickListener(this);
        tvFileTwo = findViewById(R.id.tvFileTwo);
        tvFileTwo.setOnClickListener(this);
        tvFileThree = findViewById(R.id.tvFileThree);
        tvFileThree.setOnClickListener(this);
        tvFileFour = findViewById(R.id.tvFileFour);
        tvFileFour.setOnClickListener(this);
        tvFileFive = findViewById(R.id.tvFileFive);
        tvFileFive.setOnClickListener(this);
        btnSend = findViewById(R.id.btnSend);
        btnSend.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvFileOne: {
                requestMedia(101);
                break;
            }
            case R.id.tvFileTwo: {
                requestMedia(102);
                break;
            }
            case R.id.tvFileThree: {
                requestMedia(103);
                break;
            }
            case R.id.tvFileFour: {
                requestMedia(104);
                break;
            }
            case R.id.tvFileFive: {
                requestMedia(105);
                break;
            }
            case R.id.btnSend: {
                if (etMessage.getText().toString().isEmpty()) {
                    etMessage.setError("");
                    break;
                }
                createNewOffer();
                break;
            }
        }
    }

    private void createNewOffer() {
        message = etMessage.getText().toString();

        List<Part> files = new ArrayList();
        for (int i = 0; i < selectFiles.size(); i++) {
            files.add(new FilePart("media[" + i + "]", selectFiles.get(i)));
        }

        final AlertDialog dialog = new SpotsDialog(CreateOffer.this, R.style.dialog_style);
        dialog.show();
        Ion.with(CreateOffer.this)
                .load(Constants.BASE_URL_NEW + "create-thread")
                .setMultipartParameter("text", message)
                .setMultipartParameter("customer_id", sharedPreferences.getString("id", "0"))
                .setMultipartParameter("material_id", getIntent().getExtras().getInt("materialId") + "").
                addMultipartParts(files)
                .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                dialog.dismiss();
                if (e == null) {
                    if (result.get("status").getAsBoolean()) {
                        String message = getResources().getString(R.string.successmessage);
                        Toast.makeText(CreateOffer.this, message, Toast.LENGTH_LONG).show();
                        finish();
                    } else {
                        String message = getResources().getString(R.string.somethingwrong);
                        Toast.makeText(CreateOffer.this, message, Toast.LENGTH_LONG).show();
                    }
                    return;
                }
                String message = getResources().getString(R.string.connectionerror);
                Toast.makeText(CreateOffer.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void requestMedia(int code) {
        if (ContextCompat.checkSelfPermission(CreateOffer.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            requestPermissions();
        } else {

            mediaPickerDialog(code);
        }
    }

    private void  openFilePicker(int code)
    {
        Intent intent = new Intent(this, NormalFilePickActivity.class);
        intent.putExtra(Constant.MAX_NUMBER, 1);
        intent.putExtra(NormalFilePickActivity.SUFFIX, new String[] {"xlsx", "xls", "doc", "docx", "ppt", "pptx", "pdf"});
        startActivityForResult(intent, code);
        currentCode = Constant.REQUEST_CODE_PICK_FILE;
    }
    private void openImagePicker(int code)
    {
        Intent intent = new Intent(this, ImagePickActivity.class);
        intent.putExtra(IS_NEED_CAMERA, true);
        intent.putExtra(Constant.MAX_NUMBER, 1);
        startActivityForResult(intent, code);
        currentCode = Constant.REQUEST_CODE_PICK_IMAGE;
    }
    void mediaPickerDialog(final int code)
    {
        final Dialog dialog = new Dialog(CreateOffer.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_imagepicker);

        ImageView btnFile = (ImageView)dialog.findViewById(R.id.btnFile);
        btnFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                openFilePicker(code);
                dialog.dismiss();
            }
        });
        ImageView btnImage = (ImageView)dialog.findViewById(R.id.btnImage);
        btnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                openImagePicker(code);
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }

    private void requestPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
        {
            File file = null;

            if(currentCode == Constant.REQUEST_CODE_PICK_FILE)
            {
                ArrayList<NormalFile> list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);
                file = new File(list.get(0).getPath());
            }
            else if(currentCode == Constant.REQUEST_CODE_PICK_IMAGE)
            {
                ArrayList<ImageFile> list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_IMAGE);
                file = new File(list.get(0).getPath());
            }
            selectFiles.add(file);
            if(requestCode == 101)
            {
                tvFileOne.setText(file.getName());
            }
            else if(requestCode == 102)
            {
                tvFileTwo.setText(file.getName());
            }
            else if(requestCode == 103)
            {
                tvFileThree.setText(file.getName());
            }
            else if(requestCode == 104)
            {
                tvFileFour.setText(file.getName());
            }
            else if(requestCode == 105)
            {
                tvFileFive.setText(file.getName());
            }
        }
    }


    public static String getFilePathFromURI(Context context, Uri contentUri) {
        //copy file and send new file path
        String fileName = getFileName(contentUri);
        if (!TextUtils.isEmpty(fileName)) {
            File copyFile = new File(Environment.getExternalStorageDirectory() + File.separator + fileName);
            copy(context, contentUri, copyFile);
            return copyFile.getAbsolutePath();
        }
        return null;
    }

    public static String getFileName(Uri uri) {
        if (uri == null) return null;
        String fileName = null;
        String path = uri.getPath();
        int cut = path.lastIndexOf('/');
        if (cut != -1) {
            fileName = path.substring(cut + 1);
        }
        return fileName;
    }

    public static void copy(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            IOUtils.copyStream(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
