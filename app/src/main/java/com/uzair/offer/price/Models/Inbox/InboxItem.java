package com.uzair.offer.price.Models.Inbox;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class InboxItem implements Serializable {

	@SerializedName("material")
	private Material material;

	@SerializedName("updated_at")
	private UpdatedAt updatedAt;

	@SerializedName("last-message")
	private LastMessage lastMessage;

	@SerializedName("created_at")
	private CreatedAt createdAt;

	@SerializedName("company")
	private Company company;

	@SerializedName("id")
	private int id;

	@SerializedName("customer")
	private Customer customer;

	@SerializedName("status")
	private String status;

	@SerializedName("unseen_by_customer_count")
	private int unseenByCustomerCount;

	@SerializedName("unseen_by_company_count")
	private int unseenByCompanyCount;

	public int getUnseenByCustomerCount() {
		return unseenByCustomerCount;
	}

	public void setUnseenByCustomerCount(int unseenByCustomerCount) {
		this.unseenByCustomerCount = unseenByCustomerCount;
	}

	public int getUnseenByCompanyCount() {
		return unseenByCompanyCount;
	}

	public void setUnseenByCompanyCount(int unseenByCompanyCount) {
		this.unseenByCompanyCount = unseenByCompanyCount;
	}

	public void setMaterial(Material material){
		this.material = material;
	}

	public Material getMaterial(){
		return material;
	}

	public void setUpdatedAt(UpdatedAt updatedAt){
		this.updatedAt = updatedAt;
	}

	public UpdatedAt getUpdatedAt(){
		return updatedAt;
	}

	public void setLastMessage(LastMessage lastMessage){
		this.lastMessage = lastMessage;
	}

	public LastMessage getLastMessage(){
		return lastMessage;
	}

	public void setCreatedAt(CreatedAt createdAt){
		this.createdAt = createdAt;
	}

	public CreatedAt getCreatedAt(){
		return createdAt;
	}

	public void setCompany(Company company){
		this.company = company;
	}

	public Company getCompany(){
		return company;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setCustomer(Customer customer){
		this.customer = customer;
	}

	public Customer getCustomer(){
		return customer;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"InboxItem{" +
			"material = '" + material + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",last-message = '" + lastMessage + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",company = '" + company + '\'' + 
			",id = '" + id + '\'' + 
			",customer = '" + customer + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}