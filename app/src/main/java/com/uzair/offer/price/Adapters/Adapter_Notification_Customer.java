package com.uzair.offer.price.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;
import com.uzair.offer.price.Activities.MainActivity;
import com.uzair.offer.price.Models.Model_Notification_Company;
import com.uzair.offer.price.R;

import java.util.ArrayList;

public class Adapter_Notification_Customer extends RecyclerView.Adapter<Adapter_Notification_Customer.MyViewHolder> {

    ArrayList<Model_Notification_Company> list;
    private Context context;
    public Adapter_Notification_Customer(ArrayList<Model_Notification_Company> list)
    {
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        this.context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) 
    {

        if(list.get(position).getEntertain().equalsIgnoreCase("0"))
        {
            String message = context.getResources().getString(R.string.pendinginquery);
            holder.txtnote.setText(message);
            holder.txtname.setText(list.get(position).getCompanyname());
            String url = "http://www.superpvcworks.com/www.arzsaar.com/api/"+list.get(position).getImage();
            Picasso.get().load(url).error(R.drawable.logo).placeholder(R.drawable.logo).into(holder.imageView);
            holder.btnRating.setVisibility(View.GONE);
        }
        else if(list.get(position).getEntertain().equalsIgnoreCase("1"))
        {
            String message = context.getResources().getString(R.string.answeredinquery);
            holder.txtnote.setText(message);
            holder.txtname.setText(list.get(position).getCompanyname());
            String url = "http://www.superpvcworks.com/www.arzsaar.com/api/"+list.get(position).getImage();
            Picasso.get().load(url).error(R.drawable.logo).placeholder(R.drawable.logo).into(holder.imageView);
            holder.btnRating.setVisibility(View.VISIBLE);
        }
        else if(list.get(position).getEntertain().equalsIgnoreCase("2"))
        {
            String message = context.getResources().getString(R.string.notrespondinginquery);
            holder.txtnote.setText(message);
            holder.txtname.setText(list.get(position).getCompanyname());
            String url = "http://www.superpvcworks.com/www.arzsaar.com/api/"+list.get(position).getImage();
            Picasso.get().load(url).error(R.drawable.logo).placeholder(R.drawable.logo).into(holder.imageView);
            holder.btnRating.setVisibility(View.GONE);
        }

        holder.btnRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                list.get(position).getCompanyid();
                showRatingDialog(list.get(position));
            }
        });
    }

    private void showRatingDialog(final Model_Notification_Company item)
    {
        final Dialog dialog = new Dialog(context,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_rating);

        final RatingBar ratingBar = dialog.findViewById(R.id.ratingBar);
        final Button btnSave = dialog.findViewById(R.id.btnSaveRating);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                btnSave.setText(context.getResources().getString(R.string.pleasewait));
                int rating = (int) ratingBar.getRating();
                Ion.with(context)
                        .load("http://asiffurnituremart.com/offer_price/set_rating.php")
                        .setBodyParameter("id",item.getCompanyid())
                        .setBodyParameter("rate",rating+"")
                        .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result)
                    {
                        String message = context.getResources().getString(R.string.successmessage);
                        Toast.makeText(context,message,Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });
            }
        });
        ImageButton btnClose = dialog.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }

    @Override
    public int getItemCount()
    {
        if(list != null)
        {
            return list.size();
        }
        else
        {
            return 0;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        ImageButton btnaccept, btnreject, btnRating;
        TextView txtnote, txtname, txtwait;
        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView)itemView.findViewById(R.id.item_notification_image);
            txtnote = (TextView) itemView.findViewById(R.id.item_notification_txtnote);
            txtname = (TextView) itemView.findViewById(R.id.item_notification_txtname);
            txtwait = (TextView) itemView.findViewById(R.id.item_notification_txtwait);
            btnaccept = (ImageButton) itemView.findViewById(R.id.item_notification_btnaccept);
            btnaccept.setVisibility(View.GONE);
            btnreject = (ImageButton)itemView.findViewById(R.id.item_notification_btnreject);
            btnreject.setVisibility(View.GONE);
            btnRating = itemView.findViewById(R.id.item_notification_btnrating);
        }
    }
}
