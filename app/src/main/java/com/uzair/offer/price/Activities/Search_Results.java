package com.uzair.offer.price.Activities;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.uzair.offer.price.CustomerFragments.Fragment_Search_Company;
import com.uzair.offer.price.CustomerFragments.Fragment_Search_Material;
import com.uzair.offer.price.R;

public class Search_Results extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout refresher;
    private RecyclerView rvMaterial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);
        initial();
    }


    private void initial() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getIntent().getExtras().getString("name", ""));
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        refresher = findViewById(R.id.refresher);
        refresher.setOnRefreshListener(this);
        rvMaterial = findViewById(R.id.rvMaterial);
        rvMaterial.setLayoutManager(new LinearLayoutManager(Search_Results.this));

    }

    @Override
    public void onRefresh()
    {
//        list.clear();
//        getMaterials();
    }
}
