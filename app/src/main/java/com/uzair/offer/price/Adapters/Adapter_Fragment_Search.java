package com.uzair.offer.price.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.uzair.offer.price.Activities.Material_List;
import com.uzair.offer.price.Models.Model_All_Material_List;
import com.uzair.offer.price.R;

import java.util.ArrayList;

public class Adapter_Fragment_Search extends RecyclerView.Adapter<Adapter_Fragment_Search.MyViewHolder> {

    private ArrayList<Model_All_Material_List> list;
    private Context context;
    public Adapter_Fragment_Search(ArrayList<Model_All_Material_List> list) {
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        this.context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position)
    {
        holder.txtcname.setText(list.get(position).getCompanyName());
        holder.txtmname.setText(list.get(position).getMaterialName());
        holder.checkBox.setChecked(list.get(position).isSelection());
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isselected)
            {
                list.get(position).setSelection(isselected);
            }
        });

        holder.txtcname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, Material_List.class);
                intent.putExtra("name",list.get(position).getCompanyName());
                intent.putExtra("id",list.get(position).getCompanyId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        if(list != null)
        {
            return list.size();
        }
        else
        {
            return 0;
        }

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CheckBox checkBox;
        TextView txtmname, txtcname;
        public MyViewHolder(View itemView) {
            super(itemView);
            txtmname = (TextView) itemView.findViewById(R.id.search_item_mname);
            txtcname = (TextView) itemView.findViewById(R.id.search_item_cname);
            checkBox = (CheckBox) itemView.findViewById(R.id.search_item_checkbox);
        }
    }
}
