package com.uzair.offer.price.Utils;

public class LocationConverter {


    public static String getlocation(String location)
    {
        if(location.equalsIgnoreCase("Riyadh") || location.equalsIgnoreCase("الرياض"))
        {
            return "Riyadh";
        }
        else if(location.equalsIgnoreCase("Makkah") || location.equalsIgnoreCase("مكة المكرمة"))
        {
            return "Makkah";
        }
        else if(location.equalsIgnoreCase("Madina El Monawara") || location.equalsIgnoreCase("المدينة المنورة"))
        {
            return "Madina El Monawara";
        }
        else if(location.equalsIgnoreCase("Qassim") || location.equalsIgnoreCase("القصيم"))
        {
            return "Qassim";
        }
        else if(location.equalsIgnoreCase("Eastern Region") || location.equalsIgnoreCase("الشرقية"))
        {
            return "Eastern Region";
        }
        else if(location.equalsIgnoreCase("Asir") || location.equalsIgnoreCase("عسير"))
        {
            return "Asir";
        }
        else if(location.equalsIgnoreCase("Tabuk") || location.equalsIgnoreCase("تبوك"))
        {
            return "Tabuk";
        }
        else if(location.equalsIgnoreCase("Hail") || location.equalsIgnoreCase("حائل"))
        {
            return "Hail";
        }
        else if(location.equalsIgnoreCase("Northern border area") || location.equalsIgnoreCase("منطقة الحدود الشمالية"))
        {
            return "Northern border area";
        }
        else if(location.equalsIgnoreCase("Jazan") || location.equalsIgnoreCase("جازان"))
        {
            return "Jazan";
        }
        else if(location.equalsIgnoreCase("Najran") || location.equalsIgnoreCase("نجران"))
        {
            return "Najran";
        }
        else if(location.equalsIgnoreCase("Al-Baha") || location.equalsIgnoreCase("الباحة"))
        {
            return "Al-Baha";
        }
        else if(location.equalsIgnoreCase("Al-jouf") || location.equalsIgnoreCase("الجوف"))
        {
            return "Al-jouf";
        }
        else
        {
            return "Makkah";
        }
    }
    public static String getArabicLocation(String location)
    {
        if(location.equalsIgnoreCase("Riyadh") || location.equalsIgnoreCase("الرياض"))
        {
            return "الرياض";
        }
        else if(location.equalsIgnoreCase("Makkah") || location.equalsIgnoreCase("مكة المكرمة"))
        {
            return "مكة المكرمة";
        }
        else if(location.equalsIgnoreCase("Madina El Monawara") || location.equalsIgnoreCase("المدينة المنورة"))
        {
            return "المدينة المنورة";
        }
        else if(location.equalsIgnoreCase("Qassim") || location.equalsIgnoreCase("القصيم"))
        {
            return "القصيم";
        }
        else if(location.equalsIgnoreCase("Eastern Region") || location.equalsIgnoreCase("الشرقية"))
        {
            return "الشرقية";
        }
        else if(location.equalsIgnoreCase("Asir") || location.equalsIgnoreCase("عسير"))
        {
            return "عسير";
        }
        else if(location.equalsIgnoreCase("Tabuk") || location.equalsIgnoreCase("تبوك"))
        {
            return "تبوك";
        }
        else if(location.equalsIgnoreCase("Hail") || location.equalsIgnoreCase("حائل"))
        {
            return "حائل";
        }
        else if(location.equalsIgnoreCase("Northern border area") || location.equalsIgnoreCase("منطقة الحدود الشمالية"))
        {
            return "منطقة الحدود الشمالية";
        }
        else if(location.equalsIgnoreCase("Jazan") || location.equalsIgnoreCase("جازان"))
        {
            return "جازان";
        }
        else if(location.equalsIgnoreCase("Najran") || location.equalsIgnoreCase("نجران"))
        {
            return "نجران";
        }
        else if(location.equalsIgnoreCase("Al-Baha") || location.equalsIgnoreCase("الباحة"))
        {
            return "الباحة";
        }
        else if(location.equalsIgnoreCase("Al-jouf") || location.equalsIgnoreCase("الجوف"))
        {
            return "الجوف";
        }
        else
        {
            return "مكة المكرمة";
        }
    }
}
