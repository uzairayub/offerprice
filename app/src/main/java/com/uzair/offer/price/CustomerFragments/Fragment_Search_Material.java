package com.uzair.offer.price.CustomerFragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.uzair.offer.price.Adapters.Adapter_Fragment_Search;
import com.uzair.offer.price.Models.Model_All_Material_List;
import com.uzair.offer.price.R;
import com.uzair.offer.price.Utils.Constants;
import com.uzair.offer.price.Utils.FileUtils;
import com.uzair.offer.price.Utils.LocationConverter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import dmax.dialog.SpotsDialog;
import droidninja.filepicker.FilePickerConst;

public class Fragment_Search_Material extends Fragment implements FutureCallback<JsonObject>,
        TextWatcher, View.OnClickListener {

    private RecyclerView recyclerView;
    private EditText txtsearchname;
    private View view;
    private ArrayList<Model_All_Material_List> list;
    private Button btnask;
    private SharedPreferences sharedPreferences;
    private ImageView imgerror;
    private TextView txterror;
    private Adapter_Fragment_Search adapter;
    private ViewGroup dialogTakePhotoLayout;
    private ImageView dialogimgSelected;
    private AlertDialog laodingdialog;
    private Dialog dialog;
    private ArrayList<File> selectFiles;
    private int selectedLang;
    String location ;
    private TextView dialog_acceptOffer_txtFileOne, dialog_acceptOffer_txtFileTwo, dialog_acceptOffer_txtFileThree, dialog_acceptOffer_txtFileFour, dialog_acceptOffer_txtFileFive;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search_material, container, false);
        initial();
        return view;
    }

    private void initial()
    {
        location = getActivity().getIntent().getExtras().getString("name");
        selectFiles = new ArrayList<>();
        sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        if(sharedPreferences.getString("lang","ar").equalsIgnoreCase("en"))
        {
            selectedLang = 1;
        }
        else
        {
            selectedLang = 2;
        }
        txtsearchname = (EditText) view.findViewById(R.id.fragment_searchmaterial_txtname);
        txtsearchname.addTextChangedListener(this);
        recyclerView = (RecyclerView) view.findViewById(R.id.fragment_searchmaterial_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        imgerror = (ImageView) view.findViewById(R.id.fragment_searchmaterial_img_error);
        txterror = (TextView) view.findViewById(R.id.fragment_searchmaterial_txt_error);

        btnask = (Button) view.findViewById(R.id.fragment_searchmaterial_btnask);
        btnask.setOnClickListener(this);
    }

    private void getMaterials(String name) {
        if (name.length() > 0) {
            String newlocation = LocationConverter.getlocation(location);
            Ion.with(getContext())
                    .load(Constants.BASE_URL+"searchmaterials.php")
                    .setBodyParameter("name", name)
                    .setBodyParameter("location_name", newlocation)
                    .setBodyParameter("lang",String.valueOf(selectedLang))
                    .asJsonObject().setCallback(this);
        }
    }
    private void getAllMaterials(String name) {
        if (name.length() > 0) {
            String location = getActivity().getIntent().getExtras().getString("name");
            Ion.with(getContext())
                    .load(Constants.BASE_URL+"search_material_all_comp.php")
                    .setBodyParameter("name", name)
                    .setBodyParameter("lang",String.valueOf(selectedLang))
                    .asJsonObject().setCallback(this);
        }

//        Ion.with(getContext())
//                .load("http://www.superpvcworks.com/www.arzsaar.com/api/searchmaterials.php")
//                .setMultipartParameter("name",name)
//                .setMultipartParameter("id",getActivity().getIntent().getExtras().getString("id"))
//                .asJsonObject().setCallback(this);
    }

    @Override
    public void onCompleted(Exception e, JsonObject result) {
        if (e == null) {
            if (result.get("status").getAsBoolean()) {
                JsonArray array = result.get("data").getAsJsonArray();
                if (array.size() > 0) {
                    showResults(array);
                } else {
                    String message = getResources().getString(R.string.nothing_show);
                    showError(message);
                }
            } else {
                String message = getResources().getString(R.string.nothing_show);
                showError(message);
            }


        } else {
            String message = getResources().getString(R.string.connectionerror);
            showError(message);
        }
    }

    private void printInputLanguages() {
        String currentLocale = "";
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        List<InputMethodInfo> ims = imm.getEnabledInputMethodList();

        for (InputMethodInfo method : ims) {
            List<InputMethodSubtype> submethods = imm.getEnabledInputMethodSubtypeList(method, true);
            for (InputMethodSubtype submethod : submethods) {
                if (submethod.getMode().equals("keyboard")) {
                     currentLocale = submethod.getLocale();
                    Log.i("log", "Available input method locale: " + currentLocale);
                }
            }
        }
        Log.i("log", "Available input method locale: " + currentLocale);
    }
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        printInputLanguages();
        if(location.equalsIgnoreCase(getResources().getString(R.string.search_in_all_cities)))
        {
            getAllMaterials(charSequence.toString());
            return;
        }
        getMaterials(charSequence.toString());
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.fragment_searchmaterial_btnask:
            {
                if(list != null)
                {
                    getSelectedMaterials();
                    break;
                }
                String messgae = getResources().getString(R.string.select_minimum);
                Toast.makeText(getContext(), messgae, Toast.LENGTH_LONG).show();
                break;
            }
            case R.id.dialog_acceptOffer_txtFileOne:
            {
//                pickFile(101);
                requestMedia(101);
                break;
            }
            case R.id.dialog_acceptOffer_txtFileTwo:
            {
//                pickFile(102);
                requestMedia(102);
                break;
            }
            case R.id.dialog_acceptOffer_txtFileThree:
            {
                requestMedia(103);
                break;
            }
            case R.id.dialog_acceptOffer_txtFileFour:
            {
                requestMedia(104);
                break;
            }
            case R.id.dialog_acceptOffer_txtFileFive:
            {
                requestMedia(105);
                break;
            }

        }
    }

    private void getSelectedMaterials() {
        int count = 0;
        int selectedindex = 0;
        JSONArray input = new JSONArray();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isSelection()) {
                JSONObject singleinput = new JSONObject();
                try {
                    String id = String.valueOf(list.get(i).getCompanyId());
                    singleinput.put("Id", id);
                    singleinput.put("flag", String.valueOf(false));
                    input.put(singleinput);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                selectedindex = i;
                count++;
            }
        }
        if (count >= 3) {
            String id = sharedPreferences.getString("id", "0");
            String name = sharedPreferences.getString("name", "name");
            String isasking = getResources().getString(R.string.isaskingfor);
            String message = name +" "+ isasking +" "+ list.get(selectedindex).getMaterialName();
            showDialog(id, message, input);
        } else {
            String messgae = getResources().getString(R.string.select_minimum);
            Toast.makeText(getContext(), messgae, Toast.LENGTH_LONG).show();
        }
    }

    private void send_notification_to_company(final String id, final String message,String emailMessage, final JSONArray input) {

        laodingdialog = new SpotsDialog(getContext(), R.style.dialog_style);
        laodingdialog.show();
        if (selectFiles.size() == 0)
        {
            Ion.with(getContext())
                    .load(Constants.BASE_URL+"notification_toCompany.php")
                    .setMultipartParameter("input", String.valueOf(input))
                    .setMultipartParameter("msg", message)
                    .setMultipartParameter("message",emailMessage)
                    .setMultipartParameter("cust_id", id)
                    .asString().setCallback(new FutureCallback<String>() {
                @Override
                public void onCompleted(Exception e, String result) {
                    laodingdialog.dismiss();
                    if (e == null)
                    {
                        if (result.contains("\"status\":1"))
                        {
                            String message = getResources().getString(R.string.successmessage);
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                            updateList();
                            dialog.dismiss();
                        }
                        else
                        {
                            String message = getResources().getString(R.string.somethingwrong);
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        String message = getResources().getString(R.string.connectionerror);
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }

                }
            });
        }
        if (selectFiles.size() == 1)
        {
            Ion.with(getContext())
                    .load(Constants.BASE_URL+"notification_toCompany.php")
                    .setMultipartParameter("input", String.valueOf(input))
                    .setMultipartParameter("msg", message)
                    .setMultipartParameter("message",emailMessage)
                    .setMultipartParameter("cust_id", id)
                    .setMultipartFile("image",selectFiles.get(0))
                    .asString().setCallback(new FutureCallback<String>() {
                @Override
                public void onCompleted(Exception e, String result) {
                    laodingdialog.dismiss();
                    if (e == null)
                    {
                        if (result.contains("\"status\":1"))
                        {
                            String message = getResources().getString(R.string.successmessage);
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                            updateList();
                            dialog.dismiss();
                        }
                        else
                        {
                            String message = getResources().getString(R.string.somethingwrong);
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        String message = getResources().getString(R.string.connectionerror);
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }

                }
            });
        }
        else if (selectFiles.size() == 2)
        {
            Ion.with(getContext())
                    .load(Constants.BASE_URL+"notification_toCompany.php")
                    .setMultipartParameter("input", input.toString())
                    .setMultipartParameter("msg", message)
                    .setMultipartParameter("cust_id", id)
                    .setMultipartParameter("message",emailMessage)
                    .setMultipartFile("image",selectFiles.get(0))
                    .setMultipartFile("image2",selectFiles.get(1))
                    .asString().setCallback(new FutureCallback<String>() {
                @Override
                public void onCompleted(Exception e, String result) {
                    laodingdialog.dismiss();
                    if (e == null)
                    {
                        if (result.contains("\"status\":1"))
                        {
                            String message = getResources().getString(R.string.successmessage);
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                            updateList();
                            dialog.dismiss();
                        }
                        else
                        {
                            String message = getResources().getString(R.string.somethingwrong);
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        String message = getResources().getString(R.string.connectionerror);
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }

                }
            });
        }
        else if (selectFiles.size() == 3)
        {
            Ion.with(getContext())
                    .load(Constants.BASE_URL+"notification_toCompany.php")
                    .setMultipartParameter("input", input.toString())
                    .setMultipartParameter("msg", message)
                    .setMultipartParameter("cust_id", id)
                    .setMultipartParameter("message",emailMessage)
                    .setMultipartFile("image",selectFiles.get(0))
                    .setMultipartFile("image2",selectFiles.get(1))
                    .setMultipartFile("image3",selectFiles.get(2))
                    .asString().setCallback(new FutureCallback<String>() {
                @Override
                public void onCompleted(Exception e, String result) {
                    laodingdialog.dismiss();
                    if (e == null)
                    {
                        if (result.contains("\"status\":1"))
                        {
                            String message = getResources().getString(R.string.successmessage);
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                            updateList();
                            dialog.dismiss();
                        }
                        else
                        {
                            String message = getResources().getString(R.string.somethingwrong);
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        String message = getResources().getString(R.string.connectionerror);
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }

                }
            });
        }
        else if (selectFiles.size() == 4)
        {
            Ion.with(getContext())
                    .load(Constants.BASE_URL+"notification_toCompany.php")
                    .setMultipartParameter("input", input.toString())
                    .setMultipartParameter("msg", message)
                    .setMultipartParameter("cust_id", id)
                    .setMultipartParameter("message",emailMessage)
                    .setMultipartFile("image",selectFiles.get(0))
                    .setMultipartFile("image2",selectFiles.get(1))
                    .setMultipartFile("image3",selectFiles.get(2))
                    .setMultipartFile("image4",selectFiles.get(3))
                    .asString().setCallback(new FutureCallback<String>() {
                @Override
                public void onCompleted(Exception e, String result) {
                    laodingdialog.dismiss();
                    if (e == null)
                    {
                        if (result.contains("\"status\":1"))
                        {
                            String message = getResources().getString(R.string.successmessage);
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                            updateList();
                            dialog.dismiss();
                        }
                        else
                        {
                            String message = getResources().getString(R.string.somethingwrong);
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        String message = getResources().getString(R.string.connectionerror);
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }

                }
            });
        }
        else if (selectFiles.size() == 5)
        {
            Ion.with(getContext())
                    .load(Constants.BASE_URL+"notification_toCompany.php")
                    .setMultipartParameter("input", input.toString())
                    .setMultipartParameter("msg", message)
                    .setMultipartParameter("cust_id", id)
                    .setMultipartParameter("message",emailMessage)
                    .setMultipartFile("image",selectFiles.get(0))
                    .setMultipartFile("image2",selectFiles.get(1))
                    .setMultipartFile("image3",selectFiles.get(2))
                    .setMultipartFile("image4",selectFiles.get(3))
                    .setMultipartFile("image5",selectFiles.get(4))
                    .asString().setCallback(new FutureCallback<String>() {
                @Override
                public void onCompleted(Exception e, String result) {
                    laodingdialog.dismiss();
                    if (e == null)
                    {
                        if (result.contains("\"status\":1"))
                        {
                            String message = getResources().getString(R.string.successmessage);
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                            updateList();
                            dialog.dismiss();
                        }
                        else
                        {
                            String message = getResources().getString(R.string.somethingwrong);
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        String message = getResources().getString(R.string.connectionerror);
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }

                }
            });
        }

    }

    private void showDialog(final String id, final String message, final JSONArray input) {
        dialog = new Dialog(getContext(), android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_accept_offer);

        final EditText etEmailMessage = dialog.findViewById(R.id.etEmailMessage);
        ImageButton btnclose = (ImageButton) dialog.findViewById(R.id.dialog_acceptoffer_btnclose);
        btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        Button btnSend = dialog.findViewById(R.id.dialog_acceptoffer_btnsend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                send_notification_to_company(id, message,etEmailMessage.getText().toString(), input);
            }
        });
        dialog_acceptOffer_txtFileOne = dialog.findViewById(R.id.dialog_acceptOffer_txtFileOne);
        dialog_acceptOffer_txtFileOne.setOnClickListener(this);
        dialog_acceptOffer_txtFileTwo = dialog.findViewById(R.id.dialog_acceptOffer_txtFileTwo);
        dialog_acceptOffer_txtFileTwo.setOnClickListener(this);
        dialog_acceptOffer_txtFileThree = dialog.findViewById(R.id.dialog_acceptOffer_txtFileThree);
        dialog_acceptOffer_txtFileThree.setOnClickListener(this);
        dialog_acceptOffer_txtFileFour = dialog.findViewById(R.id.dialog_acceptOffer_txtFileFour);
        dialog_acceptOffer_txtFileFour.setOnClickListener(this);
        dialog_acceptOffer_txtFileFive = dialog.findViewById(R.id.dialog_acceptOffer_txtFileFive);
        dialog_acceptOffer_txtFileFive.setOnClickListener(this);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }

    private void updateList() {
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setSelection(false);
        }
        adapter.notifyDataSetChanged();
    }


    private void showResults(JsonArray result) {
        list = new ArrayList<Model_All_Material_List>();
        Model_All_Material_List model;

        for (int i = 0; i < result.size(); i++)
        {
            JsonObject item = result.get(i).getAsJsonObject();
            model = new Model_All_Material_List();

            model.setCompanyId(item.get("company_id").getAsString());
            model.setCompanyLocation(item.get("company_location").getAsString());
            model.setCompanyName(item.get("company_name").getAsString());
            model.setMaterialName(item.get("material_name").getAsString());
            model.setSelection(false);

            list.add(model);
        }
        adapter = new Adapter_Fragment_Search(list);
        recyclerView.setAdapter(adapter);
        recyclerView.setVisibility(View.VISIBLE);
        btnask.setVisibility(View.VISIBLE);
        imgerror.setVisibility(View.GONE);
        txterror.setVisibility(View.GONE);
    }

    private void showError(String error) {
        recyclerView.setVisibility(View.GONE);
        btnask.setVisibility(View.GONE);
        imgerror.setVisibility(View.VISIBLE);
        txterror.setVisibility(View.VISIBLE);
        txterror.setText(error);
    }

    private boolean checkPermissions() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            requestPermissions();
            return false;
        }
    }

    private void requestPermissions()
    {
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
    }


//
//    public String compressImage(String imageUri) {
//
//        String filePath = getRealPathFromURI(imageUri);
//        Bitmap scaledBitmap = null;
//
//        BitmapFactory.Options options = new BitmapFactory.Options();
//
////      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
////      you try the use the bitmap here, you will get null.
//        options.inJustDecodeBounds = true;
//        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);
//
//        int actualHeight = options.outHeight;
//        int actualWidth = options.outWidth;
//
////      max Height and width values of the compressed image is taken as 816x612
//
//        float maxHeight = 816.0f;
//        float maxWidth = 612.0f;
//        float imgRatio = actualWidth / actualHeight;
//        float maxRatio = maxWidth / maxHeight;
//
////      width and height values are set maintaining the aspect ratio of the image
//
//        if (actualHeight > maxHeight || actualWidth > maxWidth) {
//            if (imgRatio < maxRatio) {
//                imgRatio = maxHeight / actualHeight;
//                actualWidth = (int) (imgRatio * actualWidth);
//                actualHeight = (int) maxHeight;
//            } else if (imgRatio > maxRatio) {
//                imgRatio = maxWidth / actualWidth;
//                actualHeight = (int) (imgRatio * actualHeight);
//                actualWidth = (int) maxWidth;
//            } else {
//                actualHeight = (int) maxHeight;
//                actualWidth = (int) maxWidth;
//
//            }
//        }
//
////      setting inSampleSize value allows to load a scaled down version of the original image
//
//        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
//
////      inJustDecodeBounds set to false to load the actual bitmap
//        options.inJustDecodeBounds = false;
//
////      this options allow android to claim the bitmap memory if it runs low on memory
//        options.inPurgeable = true;
//        options.inInputShareable = true;
//        options.inTempStorage = new byte[16 * 1024];
//
//        try {
////          load the bitmap from its path
//            bmp = BitmapFactory.decodeFile(filePath, options);
//        } catch (OutOfMemoryError exception) {
//            exception.printStackTrace();
//
//        }
//        try {
//            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
//        } catch (OutOfMemoryError exception) {
//            exception.printStackTrace();
//        }
//
//        float ratioX = actualWidth / (float) options.outWidth;
//        float ratioY = actualHeight / (float) options.outHeight;
//        float middleX = actualWidth / 2.0f;
//        float middleY = actualHeight / 2.0f;
//
//        Matrix scaleMatrix = new Matrix();
//        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
//
//        Canvas canvas = new Canvas(scaledBitmap);
//        canvas.setMatrix(scaleMatrix);
//        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));
//
////      check the rotation of the image and display it properly
//        ExifInterface exif;
//        try {
//            exif = new ExifInterface(filePath);
//
//            int orientation = exif.getAttributeInt(
//                    ExifInterface.TAG_ORIENTATION, 0);
//            Log.d("EXIF", "Exif: " + orientation);
//            Matrix matrix = new Matrix();
//            if (orientation == 6) {
//                matrix.postRotate(90);
//                Log.d("EXIF", "Exif: " + orientation);
//            } else if (orientation == 3) {
//                matrix.postRotate(180);
//                Log.d("EXIF", "Exif: " + orientation);
//            } else if (orientation == 8) {
//                matrix.postRotate(270);
//                Log.d("EXIF", "Exif: " + orientation);
//            }
//            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
//                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
//                    true);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        FileOutputStream out = null;
//        String filename = getFilename();
//        try {
//            out = new FileOutputStream(filename);
//
////          write the compressed bitmap at the destination specified by filename.
//            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        return filename;
//
//    }
//
//    public String getFilename() {
//        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/offerprice");
//        if (!file.exists()) {
//            file.mkdirs();
//        }
//        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
//        return uriSting;
//
//    }
//
//    private String getRealPathFromURI(String contentURI) {
//        Uri contentUri = Uri.parse(contentURI);
//        Cursor cursor = getActivity().getContentResolver().query(contentUri, null, null, null, null);
//        if (cursor == null) {
//            return contentUri.getPath();
//        } else {
//            cursor.moveToFirst();
//            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
//            return cursor.getString(index);
//        }
//    }
//
//    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
//        final int height = options.outHeight;
//        final int width = options.outWidth;
//        int inSampleSize = 1;
//
//        if (height > reqHeight || width > reqWidth) {
//            final int heightRatio = Math.round((float) height / (float) reqHeight);
//            final int widthRatio = Math.round((float) width / (float) reqWidth);
//            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
//        }
//        final float totalPixels = width * height;
//        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
//        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
//            inSampleSize++;
//        }
//
//        return inSampleSize;
//    }


//    private void pickFile(int code)
//    {
//        if(checkPermissions())
//        {
//            FilePickerBuilder.getInstance().setMaxCount(1).pickFile(this,code);
//        }
//        else
//        {
//            requestPermissions();
//        }
//
//    }


    private void requestMedia(int code) {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // If storage permissions are not granted, request permissions at run-time,
            // as per < API 23 guidelines.
            requestPermissions();
        } else {
            Intent intent = new Intent();

            // Pick images or videos
            intent.setType("*/*");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            {
                String[] mimeTypes = {"application/pdf","application/msword","application/vnd.ms-powerpoint","application/vnd.ms-excel","text/plain"};
//                String[] mimeTypes = {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"};
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }
            else
            {
                intent.setType("application/pdf application/msword application/vnd.ms-powerpoint application/vnd.ms-excel text/plain");
            }

            intent.setAction(Intent.ACTION_GET_CONTENT);

            // Always show the chooser (if there are multiple options available)
            startActivityForResult(Intent.createChooser(intent, "Select Media"), code);

        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);


        if(resultCode == getActivity().RESULT_OK)
        {
            Hashtable<String, Object> info = FileUtils.getFileInfo(getActivity(), data.getData());

            if (info == null) {
                Toast.makeText(getActivity(), "Extracting file information failed.", Toast.LENGTH_LONG).show();
                return;
            }

            final String path = (String) info.get("path");
            final File file = new File(path);
            selectFiles.add(file);
            if(requestCode == 101)
            {
                dialog_acceptOffer_txtFileOne.setText(file.getName());
            }
            else if(requestCode == 102)
            {
                dialog_acceptOffer_txtFileTwo.setText(file.getName());
            }
            else if(requestCode == 103)
            {
                dialog_acceptOffer_txtFileThree.setText(file.getName());
            }
            else if(requestCode == 104)
            {
                dialog_acceptOffer_txtFileFour.setText(file.getName());
            }
            else if(requestCode == 105)
            {
                dialog_acceptOffer_txtFileFive.setText(file.getName());
            }
        }
    }
}
