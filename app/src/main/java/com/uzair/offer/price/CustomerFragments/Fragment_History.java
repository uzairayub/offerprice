package com.uzair.offer.price.CustomerFragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uzair.offer.price.R;

/**
 * Created by uzairayub on 3/9/18.
 */

public class Fragment_History extends Fragment {

    private TextView tvComingSoon;
    private View view;
    private RecyclerView recyclerView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_history,container,false);
        initial();
        return view;
    }

    private void initial()
    {
        tvComingSoon = view.findViewById(R.id.tvComingSoon);
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/ElectroShackle.otf");
        tvComingSoon.setTypeface(typeface);
    }
}
