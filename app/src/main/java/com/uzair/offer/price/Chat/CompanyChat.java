package com.uzair.offer.price.Chat;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.async.http.body.FilePart;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.ion.Ion;
import com.uzair.offer.price.Activities.MainActivity;
import com.uzair.offer.price.Chat.Adapters.AdapterCompanyChat;
import com.uzair.offer.price.Models.Inbox.Company;
import com.uzair.offer.price.Models.Inbox.InboxItem;
import com.uzair.offer.price.Models.Messages.ChattingItem;
import com.uzair.offer.price.Models.Messages.ChattingResponse;
import com.uzair.offer.price.Models.Messages.SendMessageResponse;
import com.uzair.offer.price.OfferApprovel.AcceptCompanyFirst;
import com.uzair.offer.price.OfferApprovel.AcceptCompanySecond;
import com.uzair.offer.price.R;
import com.uzair.offer.price.Utils.Constants;
import com.uzair.offer.price.Utils.Util;
import com.uzair.offer.price.Utils.Utilities;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.ImagePickActivity;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.ImageFile;
import com.vincent.filepicker.filter.entity.NormalFile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import dmax.dialog.SpotsDialog;

import static com.vincent.filepicker.activity.ImagePickActivity.IS_NEED_CAMERA;

public class CompanyChat extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private BroadcastReceiver broadcastReceiver;
    private Toolbar toolbar;
    private CardView cvAttachment, cvSend;
    private EditText etMessage;
    private ArrayList<ChattingItem> list;
    private AdapterCompanyChat adapter;
    private SwipeRefreshLayout refresher;
    private RecyclerView rvMessaging;
    private ViewGroup llStatus, llApproval, llChatting;
    private TextView tvStatus;
    private ImageButton btnAccept, btnReject;
    private InboxItem thread;
    private String status, message;
    private int currentCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_chat);

        initial();
    }


    @Override
    protected void onPause() {
        super.onPause();
        Utilities.getInstance(CompanyChat.this).saveBooleanPreferences(Constants.FOREGROUND, false);
    }

    private void initial() {
        thread = (InboxItem) getIntent().getSerializableExtra("item");

        status = thread.getStatus();
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(thread.getCustomer().getCustomerName());
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        llStatus = findViewById(R.id.llStatus);
        llApproval = findViewById(R.id.llApproval);
        llChatting = findViewById(R.id.llChatting);
        tvStatus = findViewById(R.id.tvStatus);
        btnAccept = findViewById(R.id.btnAccept);
        btnAccept.setOnClickListener(this);
        btnReject = findViewById(R.id.btnReject);
        btnReject.setOnClickListener(this);

        cvAttachment = findViewById(R.id.cvAttachment);
        cvAttachment.setOnClickListener(this);
        cvSend = findViewById(R.id.cvSend);
        cvSend.setOnClickListener(this);
        etMessage = findViewById(R.id.etMessage);
        refresher = findViewById(R.id.refresher);
        refresher.setOnRefreshListener(this);
        rvMessaging = findViewById(R.id.rvMessaging);
        rvMessaging.setLayoutManager(new LinearLayoutManager(CompanyChat.this));

        list = new ArrayList<>();
        adapter = new AdapterCompanyChat(list);
        rvMessaging.setAdapter(adapter);
        refresher.setRefreshing(true);
        getMessages();
        populateData();
    }


    @Override
    public void onResume() {
        super.onResume();
        Utilities.getInstance(CompanyChat.this).saveBooleanPreferences(Constants.FOREGROUND, true);
        if (broadcastReceiver == null) {
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    ChattingItem response = (ChattingItem) intent.getSerializableExtra("message");
                    if (response.getThreadId() == thread.getId()) {
                        list.add(response);
                        adapter.notifyDataSetChanged();
                        rvMessaging.scrollToPosition(list.size() - 1);
                        return;
                    }
                    pushNotification(response.getText(), "New Message");
                }
            };
        }
        registerReceiver(broadcastReceiver, new IntentFilter("newMessage"));
    }

    private void pushNotification(String mMessage, String mTitle) {
        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        final String CHANNEL_ID = "CHANNEL_ID";
        if (Build.VERSION.SDK_INT >= 26) {  // Build.VERSION_CODES.O
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, "CHANNEL_NAME", NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(mChannel);
        }
        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("notification", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        android.support.v4.app.NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(mTitle)
                .setContentText(mMessage)
                .setAutoCancel(true)
                .setSound(uri)
                .setContentIntent(pendingIntent)
                .setContentInfo("ANY")
                .setLargeIcon(icon)
                .setColor(Color.RED)
                .setSmallIcon(R.mipmap.ic_launcher);

        notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
        notificationBuilder.setLights(Color.YELLOW, 1000, 300);
        notificationManager.notify(0, notificationBuilder.build());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
        setUnSeen();
    }

    private void setUnSeen() {
        Ion.with(CompanyChat.this)
                .load(Constants.BASE_URL_NEW + "seen-by-company")
                .setBodyParameter("thread_id", thread.getId() + "")
                .setBodyParameter("company_id", thread.getCompany().getCompanyId() + "")
                .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {

            }
        });
    }

    private void populateData() {
        Log.d("", thread.toString());
        if(status.equalsIgnoreCase("-2"))
        {
            tvStatus.setText(getString(R.string.reject_company));
            llStatus.setVisibility(View.VISIBLE);
            llApproval.setVisibility(View.GONE);
        }
        if(status.equalsIgnoreCase("-1"))
        {
            tvStatus.setText(getString(R.string.reject_customer));
            llStatus.setVisibility(View.VISIBLE);
            llApproval.setVisibility(View.GONE);
        }
        if (status.equalsIgnoreCase("0")) {
            tvStatus.setText(getString(R.string.customer_waiting));
            llStatus.setVisibility(View.VISIBLE);
        }
        if (status.equalsIgnoreCase("1")) {
            tvStatus.setText(getString(R.string.wait_from_customer));
            llStatus.setVisibility(View.VISIBLE);
            llApproval.setVisibility(View.GONE);
        }
        if (status.equalsIgnoreCase("2")) {
            tvStatus.setText(getString(R.string.customer_waiting));
            llStatus.setVisibility(View.VISIBLE);
        }
        if (status.equalsIgnoreCase("3")) {
            llChatting.setVisibility(View.VISIBLE);
        }
    }

    private void getMessages() {
        Ion.with(CompanyChat.this)
                .load(Constants.BASE_URL_NEW + "get-messages?thread_id=" + thread.getId() + "&opened_by=company")
                .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                refresher.setRefreshing(false);
                if (result.get("status").getAsBoolean()) {
                    ChattingResponse response = new Gson().fromJson(result, ChattingResponse.class);
                    list.addAll(response.getChatting());
                    adapter.notifyDataSetChanged();
                    rvMessaging.scrollToPosition(list.size() - 1);
                    return;
                }

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cvAttachment: {
                if (checkpermission()) {
                    mediaPickerDialog();
                }
                break;
            }
            case R.id.cvSend: {
                sendMessage();
                break;
            }
            case R.id.btnAccept: {
                showApproval();
                break;
            }
            case R.id.btnReject:
            {
                rejectOffer();
                break;
            }
        }
    }

    private void sendMessage() {
        message = etMessage.getText().toString();
        if (message.isEmpty()) {
            return;
        }

        ChattingItem item = new ChattingItem();
        item.setText(message);
        item.setSentBy("company");
        list.add(item);
        etMessage.setText("");
        adapter.notifyDataSetChanged();
        rvMessaging.scrollToPosition(list.size() - 1);

        Ion.with(CompanyChat.this)
                .load(Constants.BASE_URL_NEW + "send-message")
                .setBodyParameter("thread_id", thread.getId() + "")
                .setBodyParameter("sent_by", "company")
                .setBodyParameter("text", message)
                .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                if (e == null) {
                    if (result.get("status").getAsBoolean()) {

                        SendMessageResponse response = new Gson().fromJson(result, SendMessageResponse.class);
                        list.remove(list.size()-1);
                        list.add(response.getChatting());
                        adapter.notifyDataSetChanged();
                        rvMessaging.scrollToPosition(list.size()-1);
                        return;
                    }
                }
            }
        });
    }

    private void showApproval() {
        Intent intent = null;
        if (status.equals("0")) {
            intent = new Intent(CompanyChat.this, AcceptCompanyFirst.class);
            intent.putExtra("threadId", thread.getId());
            startActivityForResult(intent, 101);
        } else if (status.equals("2")) {
            intent = new Intent(CompanyChat.this, AcceptCompanySecond.class);
            intent.putExtra("threadId", thread.getId());
            startActivityForResult(intent, 102);
        }
    }


    private boolean checkpermission() {
        if (ContextCompat.checkSelfPermission(CompanyChat.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(CompanyChat.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(CompanyChat.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
            return false;
        }
    }

    @Override
    public void onRefresh() {
        list.clear();
        getMessages();
        rvMessaging.scrollToPosition(list.size() - 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 101 && resultCode == RESULT_OK) {
            llApproval.setVisibility(View.GONE);
            llStatus.setVisibility(View.GONE);
            refresher.setRefreshing(true);
            list.clear();
            getMessages();
            thread.setStatus("1");
            status = thread.getStatus();
            populateData();
        } else if (requestCode == 102 && resultCode == RESULT_OK) {
            llApproval.setVisibility(View.GONE);
            llStatus.setVisibility(View.GONE);
            refresher.setRefreshing(true);
            list.clear();
            getMessages();
            thread.setStatus("3");
            status = thread.getStatus();
            populateData();
        }
        if (resultCode == RESULT_OK) {
            File file = null;
            if (currentCode == Constant.REQUEST_CODE_PICK_FILE) {
                ArrayList<NormalFile> list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);
                file = new File(list.get(0).getPath());
            } else if (currentCode == Constant.REQUEST_CODE_PICK_IMAGE) {
                ArrayList<ImageFile> list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_IMAGE);
                file = new File(list.get(0).getPath());
            }
            sendFileMessage(file);
        }
    }

    private void openFilePicker() {
        Intent intent = new Intent(this, NormalFilePickActivity.class);
        intent.putExtra(Constant.MAX_NUMBER, 1);
        intent.putExtra(NormalFilePickActivity.SUFFIX, new String[]{"xlsx", "xls", "doc", "docx", "ppt", "pptx", "pdf"});
        startActivityForResult(intent, Constant.REQUEST_CODE_PICK_FILE);
        currentCode = Constant.REQUEST_CODE_PICK_FILE;
    }

    private void openImagePicker() {
        Intent intent = new Intent(this, ImagePickActivity.class);
        intent.putExtra(IS_NEED_CAMERA, true);
        intent.putExtra(Constant.MAX_NUMBER, 1);
        startActivityForResult(intent, Constant.REQUEST_CODE_PICK_IMAGE);
        currentCode = Constant.REQUEST_CODE_PICK_IMAGE;
    }

    void mediaPickerDialog() {
        final Dialog dialog = new Dialog(CompanyChat.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_imagepicker);

        ImageView btnFile = (ImageView) dialog.findViewById(R.id.btnFile);
        btnFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFilePicker();
                dialog.dismiss();
            }
        });
        ImageView btnImage = (ImageView) dialog.findViewById(R.id.btnImage);
        btnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openImagePicker();
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }

    private void sendFileMessage(File file)
    {
        Util.showToastMsg(CompanyChat.this, getResources().getString(R.string.uploading_file));
        Ion.with(CompanyChat.this).load(Constants.BASE_URL_NEW + "send-file")
                .setMultipartFile("file", file)
                .setMultipartParameter("thread_id", thread.getId() + "")
                .setMultipartParameter("sent_by", "company")
                .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {

                if (e != null)
                {
                    String message = getResources().getString(R.string.connectionerror);
                    Toast.makeText(CompanyChat.this, message, Toast.LENGTH_LONG).show();
                    return;
                }
                try
                {
                    SendMessageResponse response = new Gson().fromJson(result, SendMessageResponse.class);
                    if(!response.isStatus())
                    {
                        String message = getResources().getString(R.string.unableToUploadFile);
                        Toast.makeText(CompanyChat.this, message, Toast.LENGTH_LONG).show();
                        return;
                    }
                    list.add(response.getChatting());
                    adapter.notifyDataSetChanged();
                    rvMessaging.scrollToPosition(list.size() - 1);
                    return;
                }
                catch (Exception e1)
                {
                    String message = getResources().getString(R.string.connectionerror);
                    Toast.makeText(CompanyChat.this, message, Toast.LENGTH_LONG).show();
                    return;
                }
            }
        });
    }
    private void rejectOffer()
    {
        final AlertDialog dialog = new SpotsDialog(CompanyChat.this, R.style.dialog_style);
        dialog.show();
        Ion.with(CompanyChat.this)
                .load(Constants.BASE_URL_NEW+"company-reject")
                .setBodyParameter("thread_id", thread.getId()+"")
                .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {

                dialog.dismiss();
                if(e == null)
                {
                    if(result.get("status").getAsBoolean())
                    {
                        llApproval.setVisibility(View.GONE);
                        tvStatus.setText(getResources().getString(R.string.reject_company));
                    }
                    else
                    {
                        Util.showToastMsg(CompanyChat.this,getResources().getString(R.string.somethingwrong));
                    }
                    return;
                }
                String message = getResources().getString(R.string.connectionerror);
                Toast.makeText(CompanyChat.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }
}
