package com.uzair.offer.price.Activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.uzair.offer.price.Adapters.AdapterLocationMaterial;
import com.uzair.offer.price.Models.LocationMaterial.MaterialsItem;
import com.uzair.offer.price.Models.LocationMaterial.MaterialsResponse;
import com.uzair.offer.price.OfferApprovel.CreateOffer;
import com.uzair.offer.price.R;
import com.uzair.offer.price.Utils.Constants;
import com.uzair.offer.price.Utils.LocationConverter;

import java.util.ArrayList;

public class LocationMaterial extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, AdapterLocationMaterial.LocationMaterialCallback {

    private Toolbar toolbar;
    private String title;
    private ArrayList<MaterialsItem> list;
    private AdapterLocationMaterial adapter;

    private SwipeRefreshLayout refresher;
    private RecyclerView rvLocationMaterial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_material);

        initial();
    }

    private void initial()
    {
        title = getIntent().getExtras().getString("name", "");

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        refresher = findViewById(R.id.refresher);
        refresher.setOnRefreshListener(this);
        rvLocationMaterial = findViewById(R.id.rvLocationMaterial);
        rvLocationMaterial.setLayoutManager(new LinearLayoutManager(LocationMaterial.this));
        list = new ArrayList<>();
        adapter = new AdapterLocationMaterial(list, this);
        rvLocationMaterial.setAdapter(adapter);
        getLocationMaterial();
        refresher.setRefreshing(true);

    }

    private void getLocationMaterial() {
        String name = LocationConverter.getlocation(title);
        Ion.with(LocationMaterial.this)
                .load(Constants.BASE_URL_NEW + "location-materials?location_name=" + name)
                .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                refresher.setRefreshing(false);
                if (e == null) {
                    if (result.get("status").getAsBoolean())
                    {
                        MaterialsResponse materialsResponse = new Gson().fromJson(result, MaterialsResponse.class);
                        list.addAll(materialsResponse.getMaterials());
                        adapter.notifyDataSetChanged();
                    }
                    return;
                }
                String message = getResources().getString(R.string.connectionerror);
                Toast.makeText(LocationMaterial.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onRefresh() {
        list.clear();
        getLocationMaterial();
    }

    @Override
    public void onLocationSelect(int position)
    {
        Intent intent = new Intent(LocationMaterial.this, CreateOffer.class);
        intent.putExtra("materialId", list.get(position).getId());
        startActivity(intent);
    }

}
