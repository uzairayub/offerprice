package com.uzair.offer.price.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.uzair.offer.price.Models.Inbox.InboxItem;
import com.uzair.offer.price.R;
import com.uzair.offer.price.Utils.Constants;
import com.uzair.offer.price.Utils.Util;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterCustomerHistory extends RecyclerView.Adapter<AdapterCustomerHistory.MyViewHolder> {

    private ArrayList<InboxItem> list;
    private Context context;
    private CustomerHistoryCallback callback;
    private String lang;

    public AdapterCustomerHistory(ArrayList<InboxItem> list, CustomerHistoryCallback callback, String lang) {
        this.list = list;
        this.callback = callback;
        this.lang = lang;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        this.context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position)
    {
//        String url = Constants.BASE_URL_old +"companypictures/"+list.get(position).getCompany().getCompanyPicture();
//        Picasso.get().load(url).placeholder(R.drawable.logo).into(holder.ivImage);
        if(lang.equalsIgnoreCase("en"))
        {
            holder.tvMaterial.setText(list.get(position).getMaterial().getNameEng());
        }
        else
        {
            holder.tvMaterial.setText(list.get(position).getMaterial().getNameArabic());
        }

        holder.tvCompany.setText(list.get(position).getCompany().getCompanyName());
//        holder.tvLastMessage.setText(list.get(position).getLastMessage().getText());
        holder.tvTime.setText(Util.parseDate(list.get(position).getCreatedAt().getDate(), context));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onItemClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvMaterial, tvCompany, tvTime;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTime = itemView.findViewById(R.id.tvTime);
            tvMaterial = itemView.findViewById(R.id.tvMaterial);
            tvCompany = itemView.findViewById(R.id.tvCompany);
        }
    }

    public interface CustomerHistoryCallback
    {
        void onItemClicked(int position);
    }
}
