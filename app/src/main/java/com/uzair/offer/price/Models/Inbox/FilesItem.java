package com.uzair.offer.price.Models.Inbox;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FilesItem implements Serializable {

	@SerializedName("file_path")
	private String filePath;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("message_id")
	private String messageId;

	@SerializedName("id")
	private int id;

	public void setFilePath(String filePath){
		this.filePath = filePath;
	}

	public String getFilePath(){
		return filePath;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setMessageId(String messageId){
		this.messageId = messageId;
	}

	public String getMessageId(){
		return messageId;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"FilesItem{" + 
			"file_path = '" + filePath + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",message_id = '" + messageId + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}