package com.uzair.offer.price.Models.Inbox;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Material implements Serializable {

	@SerializedName("name_eng")
	private String nameEng;

	@SerializedName("id")
	private int id;

	@SerializedName("name_arabic")
	private String nameArabic;

	public void setNameEng(String nameEng){
		this.nameEng = nameEng;
	}

	public String getNameEng(){
		return nameEng;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setNameArabic(String nameArabic){
		this.nameArabic = nameArabic;
	}

	public String getNameArabic(){
		return nameArabic;
	}

	@Override
 	public String toString(){
		return 
			"Material{" + 
			"name_eng = '" + nameEng + '\'' + 
			",id = '" + id + '\'' + 
			",name_arabic = '" + nameArabic + '\'' + 
			"}";
		}
}