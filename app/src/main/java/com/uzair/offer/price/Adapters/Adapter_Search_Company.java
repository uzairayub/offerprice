package com.uzair.offer.price.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.uzair.offer.price.Activities.Material_List;
import com.uzair.offer.price.Models.Model_Company;
import com.uzair.offer.price.R;
import com.uzair.offer.price.Utils.Constants;

import java.util.ArrayList;

public class Adapter_Search_Company extends RecyclerView.Adapter<Adapter_Search_Company.MyViewHolder> {

    private ArrayList<Model_Company> list;
    private Context context;

    public Adapter_Search_Company(ArrayList<Model_Company> list) {
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_company,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position)
    {
        holder.txtname.setText(list.get(position).getName());
        holder.txtabout.setText(list.get(position).getAbout());
        holder.ratingBar.setRating(list.get(position).getRating());

        String url = Constants.BASE_URL_old+ "companypictures/"+list.get(position).getPhoto();
        Picasso.get().load(url).error(R.drawable.ic_photo_placeholder).placeholder(R.drawable.ic_photo_placeholder).into(holder.imageView);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadcompany_materials(position);
            }
        });
    }

    private void loadcompany_materials(int position)
    {
        Intent intent = new Intent(context, Material_List.class);
        intent.putExtra("name",list.get(position).getName());
        intent.putExtra("id",list.get(position).getId());
        context.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        if(list != null)
        {
            return list.size();
        }
        else
        {
            return 0;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtname, txtabout;
        ImageView imageView;
        RatingBar ratingBar;
        public MyViewHolder(View itemView) {
            super(itemView);
            txtname = (TextView)itemView.findViewById(R.id.item_search_company_name);
            txtabout= (TextView)itemView.findViewById(R.id.item_search_company_location);
            ratingBar= (RatingBar) itemView.findViewById(R.id.item_search_company_rating);
            imageView = (ImageView)itemView.findViewById(R.id.item_search_company_image);
        }
    }
}
