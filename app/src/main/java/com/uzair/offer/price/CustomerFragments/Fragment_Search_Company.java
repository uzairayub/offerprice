package com.uzair.offer.price.CustomerFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.uzair.offer.price.Adapters.Adapter_Search_Company;
import com.uzair.offer.price.Models.Model_Company;
import com.uzair.offer.price.R;
import com.uzair.offer.price.Utils.Constants;
import com.uzair.offer.price.Utils.LocationConverter;

import java.util.ArrayList;

public class Fragment_Search_Company extends Fragment implements TextWatcher, FutureCallback<JsonArray> {

     private View view;
     private RecyclerView recyclerView;
     private EditText txtname;
    private ImageView imgerror;
    private TextView txterror;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_search_company,container,false);
        initial();
        return view;
    }

    private void initial()
    {
        imgerror = (ImageView)view.findViewById(R.id.fragment_searchcompany_img_error);
        txterror = (TextView) view.findViewById(R.id.fragment_searchcompany_txt_error);

        recyclerView = (RecyclerView)view.findViewById(R.id.fragment_searchcompany_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        txtname = (EditText)view.findViewById(R.id.fragment_searchcompany_txtname);
        txtname.addTextChangedListener(this);
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {
        getcompanies(charSequence.toString());
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
    private void getcompanies(String name)
    {
        Bundle extra = getActivity().getIntent().getExtras();
        String newLocation = LocationConverter.getlocation(extra.getString("name"));
        if(name.length() > 0)
        {
            Ion.with(getContext())
                    .load(Constants.BASE_URL +"c.php")
                    .setMultipartParameter("location", newLocation)
                    .setMultipartParameter("name",name).asJsonArray().setCallback(this);
        }

    }

    @Override
    public void onCompleted(Exception e, JsonArray companies)
    {
        if(e== null)
        {
            if(companies.size() > 0)
            {
                showresults(companies);
            }
            else
            {
                String message = getResources().getString(R.string.nothing_show);
                showerror(message);
            }
        }
        else
        {
            String message = getResources().getString(R.string.connectionerror);
            showerror(message);
        }
    }

    private void showresults(JsonArray companies)
    {
        ArrayList<Model_Company> list = new ArrayList<>();
        Model_Company model;
        for(int i = 0; i < companies.size(); i++)
        {
            JsonObject company = companies.get(i).getAsJsonObject();
            model = new Model_Company();
            model.setId(company.get("company_id").getAsString());
            model.setName(company.get("company_name").getAsString());
            model.setEmail(company.get("company_email").getAsString());
            model.setAbout(company.get("company_about").getAsString());
            model.setPhoto(company.get("company_picture").getAsString());
            model.setAddress(company.get("company_address").getAsString());
            Integer rating = Integer.parseInt(company.get("rating").getAsString());
            model.setRating(rating);
            list.add(model);
        }
        recyclerView.setAdapter(new Adapter_Search_Company(list));
        recyclerView.setVisibility(View.VISIBLE);
        imgerror.setVisibility(View.GONE);
        txterror.setVisibility(View.GONE);
    }

    private void showerror(String error)
    {
        recyclerView.setVisibility(View.GONE);
        imgerror.setVisibility(View.VISIBLE);
        txterror.setVisibility(View.VISIBLE);
        txterror.setText(error);
    }
}
