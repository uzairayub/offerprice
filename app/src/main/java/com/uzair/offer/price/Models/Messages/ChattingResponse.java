package com.uzair.offer.price.Models.Messages;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ChattingResponse {

	@SerializedName("data")
	private List<ChattingItem> chatting;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private boolean status;

	public void setChatting(List<ChattingItem> chatting){
		this.chatting = chatting;
	}

	public List<ChattingItem> getChatting(){
		return chatting;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ChattingResponse{" + 
			"chatting = '" + chatting + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}