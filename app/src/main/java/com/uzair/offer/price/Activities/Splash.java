package com.uzair.offer.price.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.uzair.offer.price.Models.Inbox.InboxResponse;
import com.uzair.offer.price.R;
import com.uzair.offer.price.Utils.Constants;

import java.util.Locale;

public class Splash extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private TextView txtname;
    private boolean startAs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);
        startAs = sharedPreferences.getBoolean("startas", false);
        txtname = findViewById(R.id.splash_txtname);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/ElectroShackle.otf");
        txtname.setTypeface(typeface);

        boolean keepLogin = sharedPreferences.getBoolean("keepLogin", false);
        String lang = sharedPreferences.getString("lang", "ar");
        changelanguage(lang);
        if (keepLogin)
        {
            if (startAs) {
                getCompanyInbox();
            } else {
                getCustomerInbox();
            }
        }
        else
        {
            startActivity(new Intent(Splash.this, StartAs.class));
            finish();
        }
    }

    private void getCompanyInbox() {
        String id = sharedPreferences.getString("id", "0");
        Ion.with(Splash.this)
                .load(Constants.BASE_URL_NEW + "company-threads?company_id=" + id)
                .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                if (e == null) {
                    if (result.get("status").getAsBoolean()) {
                        InboxResponse inboxResponse = new Gson().fromJson(result, InboxResponse.class);
                        int count = 0;
                        for (int i = 0; i < inboxResponse.getData().size(); i++) {
                            if (inboxResponse.getData().get(i).getUnseenByCompanyCount() > 0) {
                                count++;
                            }
                        }
                        Intent intent = new Intent(Splash.this, MainActivity.class);
                        intent.putExtra("notification", false);
                        intent.putExtra("messageCount", count);
                        startActivity(intent);
                        finish();
                        return;
                    }
                    Intent intent = new Intent(Splash.this, MainActivity.class);
                    intent.putExtra("notification", false);
                    intent.putExtra("messageCount", 0);
                    startActivity(intent);
                    finish();
                    return;
                }
                Intent intent = new Intent(Splash.this, MainActivity.class);
                intent.putExtra("notification", false);
                intent.putExtra("messageCount", 0);
                startActivity(intent);
                finish();
            }
        });

    }

    private void getCustomerInbox() {
        String id = sharedPreferences.getString("id", "0");
        Ion.with(Splash.this)
                .load(Constants.BASE_URL_NEW+"customer-threads?customer_id="+id)
                .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                if (e == null) {
                    if (result.get("status").getAsBoolean()) {
                        InboxResponse inboxResponse = new Gson().fromJson(result, InboxResponse.class);
                        int count = 0;
                        for (int i = 0; i < inboxResponse.getData().size(); i++) {
                            if (inboxResponse.getData().get(i).getUnseenByCustomerCount() > 0) {
                                count++;
                            }
                        }
                        Intent intent = new Intent(Splash.this, MainActivity.class);
                        intent.putExtra("notification", false);
                        intent.putExtra("messageCount", count);
                        startActivity(intent);
                        finish();
                        return;
                    }
                    Intent intent = new Intent(Splash.this, MainActivity.class);
                    intent.putExtra("notification", false);
                    intent.putExtra("messageCount", 0);
                    startActivity(intent);
                    finish();
                    return;
                }
                Intent intent = new Intent(Splash.this, MainActivity.class);
                intent.putExtra("notification", false);
                intent.putExtra("messageCount", 0);
                startActivity(intent);
                finish();
                return;
            }
        });
    }

    private void changelanguage(String language) {
        String languageToLoad = language;
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }
}
