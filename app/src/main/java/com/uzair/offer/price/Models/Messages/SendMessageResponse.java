package com.uzair.offer.price.Models.Messages;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SendMessageResponse {

	@SerializedName("data")
	private ChattingItem chatting;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private boolean status;

	public ChattingItem getChatting() {
		return chatting;
	}

	public void setChatting(ChattingItem chatting) {
		this.chatting = chatting;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ChattingResponse{" + 
			"chatting = '" + chatting + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}