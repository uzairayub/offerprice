package com.uzair.offer.price.CustomerFragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.uzair.offer.price.Activities.LocationMaterial;
import com.uzair.offer.price.Activities.Search_Results;
import com.uzair.offer.price.R;

import java.util.ArrayList;

public class Fragment_Search extends Fragment implements AdapterView.OnItemClickListener, TextWatcher, View.OnClickListener {
    private View view;
    private ListView listView;
    private EditText txtname;
    private String [] values;
    private ImageView imgerror;
    private TextView txterror, tvSearchAllCities;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_search,container,false);
        initial();
        return view;
    }

    private void initial()
    {
        tvSearchAllCities = view.findViewById(R.id.tvSearchAllCities);
        tvSearchAllCities.setOnClickListener(this);
        txtname = (EditText)view.findViewById(R.id.fragment_search_txtname);
        txtname.addTextChangedListener(this);
        listView = (ListView)view.findViewById(R.id.fragment_search_listview);

        imgerror = (ImageView)view.findViewById(R.id.fragment_search_img_error);
        txterror = (TextView) view.findViewById(R.id.fragment_search_txt_error);
        values = getResources().getStringArray(R.array.cities);
        showresults(values);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
    {
        Intent intent = new Intent(getContext(), LocationMaterial.class);
        intent.putExtra("name",values[position]);
        startActivity(intent);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {
        String query = charSequence.toString();
        ArrayList<String> newlist = new ArrayList<>();
        if(query.length() > 0)
        {
            for(int j = 0; j < values.length; j++)
            {
                if(values[j].startsWith(query))
                {
                    newlist.add(values[j]);
                }
            }
            if(newlist.size()> 0)
            {
                String [] newvalues = new String[newlist.size()];
                newvalues = newlist.toArray(newvalues);
                showresults(newvalues);
            }
            else
            {
                showerror(getResources().getString(R.string.nothing_show));
            }
        }
        else
        {
            showresults(values);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private void showresults(String [] listvalues)
    {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, android.R.id.text1, listvalues);
        listView.setAdapter(adapter );
        listView.setOnItemClickListener(this);
        listView.setVisibility(View.VISIBLE);
        imgerror.setVisibility(View.GONE);
        txterror.setVisibility(View.GONE);
    }

    private void showerror(String error)
    {
        listView.setVisibility(View.GONE);
        imgerror.setVisibility(View.VISIBLE);
        txterror.setVisibility(View.VISIBLE);
        txterror.setText(error);
    }

    @Override
    public void onClick(View v)
    {
        if(v.getId() == tvSearchAllCities.getId())
        {
            Intent intent = new Intent(getContext(), Search_Results.class);
            intent.putExtra("name",getResources().getString(R.string.search_in_all_cities));
            startActivity(intent);
        }
    }
}

