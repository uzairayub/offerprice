package com.uzair.offer.price.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.uzair.offer.price.R;
import com.uzair.offer.price.Utils.Constants;
import com.uzair.offer.price.Utils.LocationConverter;

import dmax.dialog.SpotsDialog;

public class UpdateLocation extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private String location;
    private Spinner spnlocation;
    private SharedPreferences sharedPreferences;
    private AlertDialog dialog;
    private String newLocation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_location);
        initial();
    }

    private void initial()
    {
        sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        Toolbar toolbar = (Toolbar)findViewById(R.id.updatelocation_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });

        spnlocation = (Spinner)findViewById(R.id.updatelocation_spin_cities);
        populatespiner();
        spnlocation.setOnItemSelectedListener(this);

    }

    private void populatespiner()
    {
        String [] spinnerlist = getResources().getStringArray(R.array.cities);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerlist);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnlocation.setAdapter(adapter);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_updatelocation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id == R.id.menu_savelocation)
        {
            updatelocation();
        }
        return super.onOptionsItemSelected(item);
    }

    private void updatelocation()
    {
        dialog = new SpotsDialog(UpdateLocation.this, R.style.dialog_style);
        dialog.show();
        newLocation = LocationConverter.getlocation(location);
        Ion.with(UpdateLocation.this)
                .load(Constants.BASE_URL_old+"updatelocation.php")
                .setBodyParameter("id",sharedPreferences.getString("id","0"))
                .setBodyParameter("location",newLocation)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result)
            {
                dialog.dismiss();
                if(e == null)
                {
                    goBack(result);
                }
                else
                {
                    String message = getResources().getString(R.string.connectionerror);
                    Toast.makeText(UpdateLocation.this, message, Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void goBack(String result)
    {
        if(result.contains("1"))
        {

            String message = getResources().getString(R.string.successmessage);
            Toast.makeText(UpdateLocation.this, message, Toast.LENGTH_SHORT).show();
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("location",newLocation).commit();

            Intent resultIntent = new Intent();
            resultIntent.putExtra("updatedlocation", newLocation);
            setResult(Activity.RESULT_OK, resultIntent);
            finish();
        }
        else
        {
            String message = getResources().getString(R.string.somethingwrong);
            Toast.makeText(UpdateLocation.this, message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
    {
        String [] list = getResources().getStringArray(R.array.cities);
        location = list[i];
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView)
    {

    }
}
