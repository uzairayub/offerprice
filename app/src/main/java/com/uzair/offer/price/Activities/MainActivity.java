package com.uzair.offer.price.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.uzair.offer.price.CompanyFragments.FragmentAllMaterials;
import com.uzair.offer.price.CompanyFragments.FragmentCompanyInbox;
import com.uzair.offer.price.CompanyFragments.FragmentMyMaterials;
import com.uzair.offer.price.CustomerFragments.FragmentCustomerHistory;
import com.uzair.offer.price.CustomerFragments.FragmentCustomerInbox;
import com.uzair.offer.price.CustomerFragments.Fragment_Search;
import com.uzair.offer.price.Fragments.Fragment_profile;
import com.uzair.offer.price.R;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    private TextView txttitle, tvMessageCount;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private static boolean startas;
    private SharedPreferences sharedPreferences;
    private boolean notification;
    private int messageCount = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        headerinitial();
    }
    private void headerinitial()
    {
        messageCount = getIntent().getExtras().getInt("messageCount",0);
        notification = getIntent().getExtras().getBoolean("notification",false);
        sharedPreferences = getSharedPreferences("user",MODE_PRIVATE);
        startas = sharedPreferences.getBoolean("startas",false);
        txttitle = (TextView)findViewById(R.id.maintoolbar_title);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/ElectroShackle.otf");
        txttitle.setTypeface(typeface);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        tvMessageCount = findViewById(R.id.tvMessageCount);
        tvMessageCount.setText(String.valueOf(messageCount));
        if(messageCount > 0)
            tvMessageCount.setVisibility(View.VISIBLE);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.getTabAt(0).setIcon(geticon(R.drawable.ic_list_f,R.drawable.ic_search_f));
        tabLayout.getTabAt(1).setIcon(geticon(R.drawable.ic_add,R.drawable.ic_list));
        tabLayout.addOnTabSelectedListener(this);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        updatefragment();
    }

    private void updatefragment()
    {
        if(notification)
        {
            mViewPager.setCurrentItem(3);
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab)
    {
        settabicon_select(tab);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab)
    {
        settabicon_unselect(tab);
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab)
    {

    }

    private void settabicon_select(TabLayout.Tab tab)
    {
        if(tab.getPosition() == 0)
        {
            tab.setIcon(geticon(R.drawable.ic_list_f,R.drawable.ic_search_f));
        }
        else if (tab.getPosition() == 1)
        {
            tab.setIcon(geticon(R.drawable.ic_add_f,R.drawable.ic_list_f));
        }
        else if (tab.getPosition() == 2)
        {
            tab.setIcon(R.drawable.ic_user_f);
        }
        else if (tab.getPosition() == 3)
        {
            tab.setIcon(R.drawable.ic_message_s);
            tvMessageCount.setVisibility(View.GONE);
        }
    }
    private void settabicon_unselect(TabLayout.Tab tab)
    {
        if(tab.getPosition() == 0)
        {
            tab.setIcon(geticon(R.drawable.ic_list,R.drawable.ic_msearch));
        }
        else if (tab.getPosition() == 1)
        {
            tab.setIcon(geticon(R.drawable.ic_add,R.drawable.ic_list));
        }
        else if (tab.getPosition() == 2)
        {
            tab.setIcon(R.drawable.ic_user);
        }
        else if (tab.getPosition() == 3)
        {
            tab.setIcon(R.drawable.ic_message);
        }
    }

    private int geticon(int company, int customer)
    {
        if(startas)
        {
            return company;
        }
        else
        {
            return customer;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id == R.id.menu_settings)
        {
            showdialouge();
        }
        else if (id == R.id.menu_logout)
        {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear().commit();
            startActivity(new Intent(MainActivity.this,Splash.class));
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public static class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position)
        {
            if(position == 0)
            {
                return getFragmeent(new FragmentMyMaterials(), new Fragment_Search());
            }
            else if(position == 1)
            {
//                return new FragmentAllMaterials();
                return getFragmeent(new FragmentAllMaterials(), new FragmentCustomerHistory());

            }
            else if(position == 2)
            {
                return new Fragment_profile();
            }
            else
            {
                return getFragmeent(new FragmentCompanyInbox(), new FragmentCustomerInbox());
            }
        }

        private Fragment getFragmeent(Fragment company, Fragment customer)
        {
            if(startas)
            {
                return company;
            }
            else
            {
                return customer;
            }
        }

        @Override
        public int getCount() {
            return 4;
        }

    }

    void showdialouge ()
    {
        final Dialog dialog = new Dialog(MainActivity.this,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_setting);

        ImageButton btnclose = (ImageButton)dialog.findViewById(R.id.dialog_language_btnclose);
        final TextView txtarabic = (TextView)dialog.findViewById(R.id.dialog_language_arabic);
        final TextView txtenglish = (TextView)dialog.findViewById(R.id.dialog_language_english);
        setcolor(txtarabic, txtenglish);
        txtenglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                changelanguage("en");
                setcolor(txtarabic, txtenglish);
                restartapp();
            }
        });
        txtarabic.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                changelanguage("ar");
                setcolor(txtarabic, txtenglish);
                restartapp();
            }
        });
        btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }

    private void restartapp()
    {
        finish();
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage( getBaseContext().getPackageName() );
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    private void setcolor(TextView txtarabic, TextView txtenglish)
    {
        String lan = sharedPreferences.getString("lang","ar");
        if(lan.equalsIgnoreCase("en"))
        {
            txtenglish.setTextColor(Color.parseColor("#fd001e"));
            txtarabic.setTextColor(Color.BLACK);
            txtenglish.setCompoundDrawablesWithIntrinsicBounds (R.drawable.ic_lines,0,R.drawable.ic_tick,0);
            txtarabic.setCompoundDrawablesWithIntrinsicBounds (R.drawable.ic_lines,0,0,0);
        }
        else
        {
            txtarabic.setTextColor(Color.parseColor("#fd001e"));
            txtenglish.setTextColor(Color.BLACK);
            txtarabic.setCompoundDrawablesWithIntrinsicBounds (R.drawable.ic_lines,0,R.drawable.ic_tick,0);
            txtenglish.setCompoundDrawablesWithIntrinsicBounds (R.drawable.ic_lines,0,0,0);
        }
    }

    private void changelanguage(String language)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("lang",language).commit();
        String languageToLoad  = language;
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,getBaseContext().getResources().getDisplayMetrics());
    }

}
