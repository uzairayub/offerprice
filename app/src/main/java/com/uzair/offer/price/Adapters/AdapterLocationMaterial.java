package com.uzair.offer.price.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uzair.offer.price.Models.LocationMaterial.MaterialsItem;
import com.uzair.offer.price.R;

import java.util.ArrayList;

public class AdapterLocationMaterial extends RecyclerView.Adapter<AdapterLocationMaterial.MyViewHolder> {

    private ArrayList<MaterialsItem> list;
    private LocationMaterialCallback callback;
    private SharedPreferences sharedPreferences;
    private Context context;

    public AdapterLocationMaterial(ArrayList<MaterialsItem> list, LocationMaterialCallback callback) {
        this.list = list;
        this.callback = callback;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_material, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position)
    {
        sharedPreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);
        if(sharedPreferences.getString("lang","ar").equalsIgnoreCase("en"))
        {
            holder.tvMaterialName.setText(list.get(position).getNameEng());
        }
        else
        {
            holder.tvMaterialName.setText(list.get(position).getNameArabic());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onLocationSelect(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvMaterialName;
        ViewGroup llSelection;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvMaterialName = itemView.findViewById(R.id.tvMaterialName);
            llSelection = itemView.findViewById(R.id.llSelection);
            llSelection.setVisibility(View.GONE);
        }
    }

    public interface LocationMaterialCallback
    {
        void onLocationSelect(int position);
    }
}
