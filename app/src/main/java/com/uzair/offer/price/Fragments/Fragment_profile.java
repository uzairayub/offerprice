package com.uzair.offer.price.Fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;
import com.uzair.offer.price.Activities.UpdateLocation;
import com.uzair.offer.price.R;
import com.uzair.offer.price.Utils.Constants;
import com.uzair.offer.price.Utils.LocationConverter;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.ImagePickActivity;
import com.vincent.filepicker.filter.entity.ImageFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import dmax.dialog.SpotsDialog;

import static com.vincent.filepicker.activity.ImagePickActivity.IS_NEED_CAMERA;

public class Fragment_profile extends Fragment implements View.OnClickListener, FutureCallback<JsonObject>{

    private SharedPreferences sharedPreferences;
    private ImageView imageView;
    private ImageButton btnadd, btnEditLocation;
    private EditText txtname, txtabout, txtaddress;
    private TextView txtemail, txtlocation;
    private Button btnupdate;
    private AlertDialog dialog;
    private View view;
    private boolean startas;
    private LinearLayout llLocation;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile,container,false);
        initial();
        return view;
    }

    private void initial()
    {
        sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        startas = sharedPreferences.getBoolean("startas",false);
        imageView = (ImageView)view.findViewById(R.id.fragment_profile_image);
        btnadd = (ImageButton)view.findViewById(R.id.fragment_profile_btnadd);
        btnadd.setOnClickListener(this);
        txtname = (EditText)view.findViewById(R.id.fragment_profile_name);
        txtabout = (EditText)view.findViewById(R.id.fragment_profile_about);
        txtemail = (TextView)view.findViewById(R.id.fragment_profile_email);
        txtaddress = (EditText)view.findViewById(R.id.fragment_profile_address);
        btnupdate = (Button)view.findViewById(R.id.fragment_profile_btnupdate);
        btnupdate.setOnClickListener(this);
        txtlocation = (TextView)view.findViewById(R.id.fragment_profile_location);
        txtlocation.setOnClickListener(this);
        llLocation = view.findViewById(R.id.llLocation);
        btnEditLocation = view.findViewById(R.id.btnEditLocation);
        btnEditLocation.setOnClickListener(this);
        getolddate();

    }

    private void getolddate()
    {
        txtemail.setText(sharedPreferences.getString("email",""));
        txtname.setText(sharedPreferences.getString("name",""));
        txtabout.setText(sharedPreferences.getString("about",""));
        txtaddress.setText(sharedPreferences.getString("address",""));
        txtlocation.setText(getLocationName());
        String photo = sharedPreferences.getString("photo","");
        if(photo.contains("."))
        {
            if(startas)
            {
                photo = Constants.BASE_URL_old +"companypictures/"+photo;
            }
            else
            {
                photo = Constants.BASE_URL_old+"customerpictures/"+photo;
            }
            Picasso.get().load(photo).placeholder(R.drawable.user_placeholder).error(R.drawable.user_placeholder).into(imageView);
        }

        if(!startas)
        {
           llLocation.setVisibility(View.GONE);
        }
    }

    private String getLocationName()
    {
        String location = sharedPreferences.getString("location","");
        String selectedLanguage = sharedPreferences.getString("lang","ar");
        if(selectedLanguage.equalsIgnoreCase("ar"))
        {
            return LocationConverter.getArabicLocation(location);
        }
        return location;
    }

    @Override
    public void onClick(View view)
    {
        if(view.getId() == btnadd.getId())
        {
            if(checkpermission())
            {
                openImagePicker();
            }
        }
        else if (view.getId() == btnupdate.getId())
        {
            updateprofile();
        }
        else if(view.getId() == txtlocation.getId())
        {
            startActivityForResult(new Intent(getContext(), UpdateLocation.class),101);
        }
        else if(view.getId() == btnEditLocation.getId())
        {
            startActivityForResult(new Intent(getContext(), UpdateLocation.class),101);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == getActivity().RESULT_OK && requestCode == Constant.REQUEST_CODE_PICK_IMAGE)
        {
            ArrayList<ImageFile> list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_IMAGE);
            onImagePicked(new File(list.get(0).getPath()));
        }
        if(requestCode == 101 && resultCode == getActivity().RESULT_OK)
        {
            txtlocation.setText(getLocationName());
        }
    }

    private void updateprofile()
    {
        String url = geturl();
        dialog = new SpotsDialog(getContext(), R.style.dialog_style);
        dialog.show();
        Ion.with(getContext())
                .load(url)
                .setMultipartParameter("id",sharedPreferences.getString("id","0"))
                .setMultipartParameter("name",txtname.getText().toString())
                .setMultipartParameter("email",txtemail.getText().toString())
                .setMultipartParameter("about",txtabout.getText().toString())
                .setMultipartParameter("address",txtaddress.getText().toString())
                .asJsonObject().setCallback(this);
    }

    @Override
    public void onCompleted(Exception e, JsonObject result)
    {
        dialog.dismiss();
        if(e == null)
        {
            if(result.get("status").getAsString().equalsIgnoreCase("1"))
            {
                String message = getResources().getString(R.string.successmessage);
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                updateSharedPreferences();
            }
            else
            {
                String message = getResources().getString(R.string.somethingwrong);
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
        else
        {
            String message = getResources().getString(R.string.connectionerror);
            Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            Log.d("","");
        }
    }

    private void updateSharedPreferences()
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("name",txtname.getText().toString());
        editor.putString("email",txtemail.getText().toString());
        editor.putString("about",txtabout.getText().toString());
        editor.putString("address",txtaddress.getText().toString());
        editor.commit();

    }

    private String geturl()
    {
        if(startas)
        {
            return Constants.BASE_URL_old+"company_update.php";
        }
        else
        {
            return Constants.BASE_URL_old+"customer_update.php";
        }
    }

    private boolean checkpermission()
    {
        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
        {
            return true;
        }
        else
        {
            requespermission();
            return false;
        }
    }

    private void requespermission()
    {
        ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},101);
    }


    public void onImagePicked(File imageFile)
    {
        Bitmap myBitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
        imageView.setImageBitmap(myBitmap);
        dialog = new SpotsDialog(getContext(), R.style.dialog_style);
        dialog.show();
        String url = getphotourl();
        String photo = compressImage(String.valueOf(imageFile));
        File file = new File(photo);
        Ion.with(getContext())
                .load(url)
                .setMultipartParameter("id",sharedPreferences.getString("id","0"))
                .setMultipartFile("picture",file)
                .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result)
            {
                dialog.dismiss();
                if(e == null)
                {
                    if(result.get("status").getAsString().equalsIgnoreCase("1"))
                    {
                        String message = getResources().getString(R.string.successmessage);
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                        updateSharedPreferences_photo(result.get("data").getAsString());
                    }
                    else
                    {
                        imageView.setImageResource(R.drawable.user_placeholder);
                        String message = getResources().getString(R.string.somethingwrong);
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    imageView.setImageResource(R.drawable.user_placeholder);
                    String message = getResources().getString(R.string.connectionerror);
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void updateSharedPreferences_photo(String s)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("photo",s).commit();
    }
    private String getphotourl()
    {
        if(startas)
        {
            return Constants.BASE_URL_old+"company_pictureupdate.php";
        }
        else
        {
            return Constants.BASE_URL_old+"customer_pictureupdate.php";
        }
    }

    private void openImagePicker()
    {
        Intent intent = new Intent(getActivity(), ImagePickActivity.class);
        intent.putExtra(IS_NEED_CAMERA, true);
        intent.putExtra(Constant.MAX_NUMBER, 1);
        startActivityForResult(intent, Constant.REQUEST_CODE_PICK_IMAGE);
    }
//    void showDialog()
//    {
//        final Dialog dialog = new Dialog(getContext(),android.R.style.Theme_Translucent_NoTitleBar);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_imagepicker);
//
//        ImageView btncamera = (ImageView)dialog.findViewById(R.id.dialog_imagepicker_camera);
//        btncamera.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view)
//            {
//                openImagePicker();
//                dialog.dismiss();
//            }
//        });
//        ImageView btngallry = (ImageView)dialog.findViewById(R.id.dialog_imagepicker_gallry);
//        btngallry.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view)
//            {
//                openImagePicker();
//                dialog.dismiss();
//            }
//        });
//
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(dialog.getWindow().getAttributes());
//        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        lp.gravity = Gravity.CENTER;
//        dialog.getWindow().setAttributes(lp);
//        dialog.show();
//    }

    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public String getFilename()
    {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/offerprice");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getActivity().getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }
}
