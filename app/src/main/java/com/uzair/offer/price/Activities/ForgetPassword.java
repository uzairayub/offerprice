package com.uzair.offer.price.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.uzair.offer.price.R;
import com.uzair.offer.price.Utils.Constants;

import dmax.dialog.SpotsDialog;

public class ForgetPassword extends AppCompatActivity implements View.OnClickListener {

    private ViewGroup codelayout, passwordlayout;
    private TextView txtalreadyhavecode;
    private Button btnsend, btnupdate;
    private EditText txtemail, txtcode, txtpassword, txtcpassword;
    private AlertDialog loadingdialog;
    private SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        initial();
    }

    private void initial()
    {
        sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        Toolbar toolbar = (Toolbar)findViewById(R.id.forgetpassword_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        codelayout = (ViewGroup) findViewById(R.id.forgetpassword_codelayout);
        passwordlayout = (ViewGroup) findViewById(R.id.forgetpassword_passwordlayout);
        passwordlayout.setVisibility(View.GONE);

        txtalreadyhavecode = (TextView)findViewById(R.id.forgetpassword_txtavailalecode);
        txtalreadyhavecode.setOnClickListener(this);

        btnsend = (Button) findViewById(R.id.forgetpassword_btnsend);
        btnsend.setOnClickListener(this);
        btnupdate = (Button) findViewById(R.id.forgetpassword_btnupdate);
        btnupdate.setOnClickListener(this);

        txtemail = (EditText) findViewById(R.id.forgetpassword_txtemail);
        txtcode = (EditText) findViewById(R.id.forgetpassword_txtcode);
        txtpassword = (EditText) findViewById(R.id.forgetpassword_txtpassowrd);
        txtcpassword = (EditText) findViewById(R.id.forgetpassword_txtcpassowrd);
    }

    @Override
    public void onClick(View view)
    {
        if (view.getId() == txtalreadyhavecode.getId())
        {
            codelayout.setVisibility(View.GONE);
            passwordlayout.setVisibility(View.VISIBLE);
        }
        else if(view.getId() == btnsend.getId())
        {
            if(!txtemail.getText().toString().trim().isEmpty())
            {
                sendemail();
            }
        }
        else if(view.getId() == btnupdate.getId())
        {
            if(validation())
            {
                updatepassword();
            }
        }
    }

    private void sendemail()
    {
        loadingdialog = new SpotsDialog(ForgetPassword.this, R.style.dialog_style);
        loadingdialog.show();
        Ion.with(ForgetPassword.this)
                .load(Constants.BASE_URL +"forget_password.php")
                .setBodyParameter("email",txtemail.getText().toString())
                .setBodyParameter("user_type",getusertype())
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result)
                    {
                        loadingdialog.dismiss();
                        if(e == null)
                        {
                            if(result.get("status").getAsString().equals("1"))
                            {
                                String message = getResources().getString(R.string.successmessage);
                                Toast.makeText(ForgetPassword.this, message, Toast.LENGTH_SHORT).show();
                                codelayout.setVisibility(View.GONE);
                                passwordlayout.setVisibility(View.VISIBLE);
                            }
                        }
                        else
                        {
                            String message = getResources().getString(R.string.connectionerror);
                            Toast.makeText(ForgetPassword.this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }

    private void updatepassword()
    {
        loadingdialog = new SpotsDialog(ForgetPassword.this, R.style.dialog_style);
        loadingdialog.show();
        Ion.with(ForgetPassword.this)
                .load(Constants.BASE_URL+"set_newPassword.php")
                .setBodyParameter("password",txtpassword.getText().toString())
                .setBodyParameter("code",txtcode.getText().toString())
                .setBodyParameter("user_type",getusertype())
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result)
                    {
                        loadingdialog.dismiss();
                        if(e == null)
                        {
                            if(result.get("status").getAsString().equals("1"))
                            {
                                String message = getResources().getString(R.string.successmessage);
                                Toast.makeText(ForgetPassword.this, message, Toast.LENGTH_SHORT).show();
                                finish();
                            }
                            else
                            {
                                String message = getResources().getString(R.string.somethingwrong);
                                Toast.makeText(ForgetPassword.this,message , Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            String message = getResources().getString(R.string.connectionerror);
                            Toast.makeText(ForgetPassword.this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private boolean validation()
    {
        String code = txtcode.getText().toString().trim();
        String password = txtpassword.getText().toString().trim();
        String cpassword = txtcpassword.getText().toString().trim();

        if(code.isEmpty())
        {
            txtcode.setError("Please enter secret code");
            return false;
        }
        if(password.isEmpty())
        {
            txtcode.setError("Please enter Password");
            return false;
        }
        if(cpassword.isEmpty())
        {
            txtcode.setError("Please confirm your password");
            return false;
        }
        if(!password.equals(cpassword))
        {
            txtpassword.setError("No matching password");
            return false;
        }
        return true;
    }
    private String getusertype()
    {
        if(sharedPreferences.getBoolean("startas",false))
        {
            return "1";
        }
        else
        {
            return "2";
        }
    }
}
