package com.uzair.offer.price.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.uzair.offer.price.Others.GPSTracker;
import com.uzair.offer.price.R;
import com.uzair.offer.price.Utils.Constants;
import com.uzair.offer.price.Utils.LocationConverter;
import com.uzair.offer.price.Utils.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import dmax.dialog.SpotsDialog;

public class SignUp extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private TextView txtsignin, txttitle;
    private EditText txtemail, txtpassword, txtcpassword, txtname;
    private Button btnsignup;
    private StringRequest stringRequest;
    private AlertDialog dialog;
    private SharedPreferences sharedPreferences;
    private GPSTracker gpsTracker;
    private boolean startas;
    private String token;
    private Spinner spnlocation;
    private String mylocation = "Lahore";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        createToken();
        initial();
    }


    private void createToken()
    {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( SignUp.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                token = instanceIdResult.getToken();
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void initial()
    {
        sharedPreferences = getSharedPreferences("user",MODE_PRIVATE);
        startas = getIntent().getExtras().getBoolean("startas");

        txttitle = (TextView)findViewById(R.id.signup_txttitle);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/ElectroShackle.otf");
        txttitle.setTypeface(typeface);
        txtsignin = (TextView)findViewById(R.id.signup_txtsignin);
        txtsignin.setOnClickListener(this);

        txtemail = (EditText)findViewById(R.id.signup_txt_email);
        txtpassword = (EditText)findViewById(R.id.signup_txt_password);
        txtcpassword= (EditText)findViewById(R.id.signup_txt_cpassword);
        txtname= (EditText)findViewById(R.id.signup_txt_name);

        btnsignup = (Button)findViewById(R.id.signup_btnsignup);
        btnsignup.setOnClickListener(this);

        spnlocation = (Spinner)findViewById(R.id.signup_spin_cities);
        populatespiner();
        spnlocation.setOnItemSelectedListener(this);
    }

    private void populatespiner()
    {
        String [] spinnerlist = getResources().getStringArray(R.array.cities);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerlist);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnlocation.setAdapter(adapter);
    }

    private  void volly_signuprequest()
    {

        dialog = new SpotsDialog(SignUp.this, R.style.dialog_style);
        dialog.show();

        Ion.with(SignUp.this)
                .load(geturl())
                .setBodyParameter("email",txtemail.getText().toString())
                .setBodyParameter("password",txtpassword.getText().toString())
                .setBodyParameter("name",txtname.getText().toString())
                .setBodyParameter("token",token)
                .setBodyParameter("location_name",mylocation)
                .setBodyParameter("lang",getSelectedLang())
                .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result)
            {
                if(result.get("status").getAsString().equalsIgnoreCase("success") || result.get("status").getAsString().equalsIgnoreCase("1") )
                {
                    String message = getResources().getString(R.string.successmessage);
                    Toast.makeText(SignUp.this,message,Toast.LENGTH_LONG).show();
                    if(startas)
                    {
                        getcompany(result);
                    }
                    else
                    {
                        getcustomer(result);
                    }
                }
                else
                {
                    String message = getResources().getString(R.string.useralreadyexist);
                    Toast.makeText(SignUp.this,message,Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void getcustomer(JsonObject respons)
    {
        JsonObject data = respons.get("data").getAsJsonObject();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("id",data.get("customer_id").getAsString());
        editor.putString("name",data.get("customer_name").getAsString());
        editor.putString("email",data.get("customer_email").getAsString());
        editor.putString("about",data.get("customer_about").getAsString());
        editor.putString("photo",data.get("customer_picture").getAsString());
        editor.putString("address",data.get("customer_address").getAsString());
        editor.putString("location",data.get("location_name").getAsString());
        editor.commit();
        startscreen();
        finish();
    }

    private void getcompany(JsonObject response)
    {
        JsonObject data = response.get("data").getAsJsonObject();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("id",data.get("company_id").getAsString());
        editor.putString("name",data.get("company_name").getAsString());
        editor.putString("email",data.get("company_email").getAsString());
        editor.putString("about",data.get("company_about").getAsString());
        editor.putString("photo",data.get("company_picture").getAsString());
        editor.putString("address",data.get("company_address").getAsString());
        editor.putString("location",data.get("location_name").getAsString());
        editor.commit();
        startscreen();
        finish();
    }

    private String getSelectedLang()
    {
        String lang = sharedPreferences.getString("lang","ar");
        if(lang.equalsIgnoreCase("ar"))
        {
            return "2";
        }
        return "1";
    }
    @Override
    public void onClick(View view)
    {
        if(view.getId() == btnsignup.getId())
        {
            if(check_validation())
            {
//                if(mylocation != null && !mylocation.trim().isEmpty())
//                {

                    volly_signuprequest();
//                }
//                else
//                {
//                    getlocaion();
//                }
            }
        }
        else if(view.getId() == txtsignin.getId())
        {
            finish();
        }

    }
//
//    private void getlocaion()
//    {
//        if(checkpermission())
//        {
//            gpsTracker = new GPSTracker(SignUp.this);
//            if(gpsTracker.canGetLocation())
//            {
//                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
//                List<Address> addresses = null;
//                try
//                {
//                    addresses = geocoder.getFromLocation(gpsTracker.getLatitude(), gpsTracker.getLongitude(), 1);
//                    if(addresses.size() > 0)
//                    {
////                        cityname = addresses.get(0).getLocality();
//                        btnsignup.performClick();
//                    }
//                    else
//                    {
//                        btnsignup.performClick();
//                    }
//
//                }
//                catch (IOException e)
//                {
//                    e.printStackTrace();
//                    Toast.makeText(SignUp.this, "Please wait getting your location", Toast.LENGTH_LONG).show();
//                }
//            }
//            else
//            {
//                gpsTracker.showSettingsAlert();
//                Toast.makeText(SignUp.this, "Please Enable your location", Toast.LENGTH_LONG).show();
//            }
//        }
//        else
//        {
//            getpermission();
//        }
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
        {
            btnsignup.performClick();
        }
        else
        {
            getpermission();
        }
    }

    private void getpermission()
    {
        ActivityCompat.requestPermissions(SignUp.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},101);
    }

    private boolean checkpermission()
    {
        if(ContextCompat.checkSelfPermission(SignUp.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private String geturl()
    {
        if(startas)
        {
            return Constants.BASE_URL_old +"company_signup.php";
        }
        else
        {
            return Constants.BASE_URL_old +"customer_signup.php";
        }
    }

    private boolean check_validation()
    {
        if(!txtemail.getText().toString().isEmpty())
        {
            if (Util.isValidEmail(txtemail.getText().toString())) {
                if (Util.isValidPassword(txtpassword.getText().toString()))
                {
                    if(txtpassword.getText().toString().equals(txtcpassword.getText().toString()))
                    {
                        return true;
                    }
                    else
                    {
                        txtcpassword.setError(getResources().getString(R.string.no_matching_password));
                        return false;
                    }
                }
                else
                {
                    txtpassword.setError(getResources().getString(R.string.shot_password));
                    return false;
                }

            }
            else
            {
                txtemail.setError(getResources().getString(R.string.valid_email));
                return false;
            }
        }
        else
        {
            txtname.setError(getResources().getString(R.string.name));
            return false;
        }
    }

    private void startscreen()
    {
        Intent intent = new Intent(SignUp.this,MainActivity.class);
        intent.putExtra("notification",false);
        startActivity(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
    {
        String [] list = getResources().getStringArray(R.array.cities);
        mylocation = list[i];
        mylocation = LocationConverter.getlocation(mylocation);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
