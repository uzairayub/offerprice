package com.uzair.offer.price.Adapters;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;
import com.uzair.offer.price.Activities.LogIn;
import com.uzair.offer.price.Activities.MainActivity;
import com.uzair.offer.price.Interfaces.InterfaceAcceptOffer;
import com.uzair.offer.price.Models.Model_Notification_Company;
import com.uzair.offer.price.R;
import com.uzair.offer.price.Utils.Constants;

import java.util.ArrayList;

import dmax.dialog.SpotsDialog;

public class Adapter_Notification_Company extends RecyclerView.Adapter<Adapter_Notification_Company.MyViewHolder>
{

    ArrayList<Model_Notification_Company> list;
    InterfaceAcceptOffer interfaceAcceptOffer;
    private Context context;
    private String id;
    public Adapter_Notification_Company(ArrayList<Model_Notification_Company> list, InterfaceAcceptOffer interfaceAcceptOffer, String id)
    {
        this.list = list;
        this.interfaceAcceptOffer = interfaceAcceptOffer;
        this.id = id;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        this.context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) 
    {
        holder.txtnote.setText(list.get(position).getMessage());
        holder.txtname.setText(list.get(position).getCustomername());
        String url = "http://www.superpvcworks.com/www.arzsaar.com/api/"+list.get(position).getImage();
        Picasso.get().load(url).error(R.drawable.logo).placeholder(R.drawable.logo).into(holder.imageView);
        holder.btnaccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                accept(position);
            }
        });
        holder.btnreject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reject(position);
            }
        });
    }

    private void reject(final int position)
    {
        final AlertDialog laodingdialog = new SpotsDialog(context, R.style.dialog_style);
        laodingdialog.show();
        Ion.with(context)
                .load(Constants.BASE_URL+"notification_reject.php")
                .setMultipartParameter("notification_id",list.get(position).getNotificaationid())
                .setMultipartParameter("customer_id", list.get(position).getCustomerid())
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result)
            {
                laodingdialog.dismiss();
                if(e == null)
                {
                    if(result.contains("success\\\":1"))
                    {
                        list.remove(position);
                        notifyDataSetChanged();
                        String message = context.getResources().getString(R.string.successmessage);
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        String message = context.getResources().getString(R.string.somethingwrong);
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    String message = context.getResources().getString(R.string.connectionerror);
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                }


            }
        });
    }

    private void accept(final int position)
    {
        final AlertDialog laodingdialog = new SpotsDialog(context, R.style.dialog_style);
        laodingdialog.show();
        Ion.with(context)
                .load(Constants.BASE_URL+"notification_accept.php")
                .setMultipartParameter("notification_id",list.get(position).getNotificaationid())
                .setMultipartParameter("customer_id", list.get(position).getCustomerid())
                .setMultipartParameter("company_id",id)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result)
            {
                laodingdialog.dismiss();
                if(e == null)
                {
                    if(result.contains("success\":1"))
                    {
                        list.remove(position);
                        notifyDataSetChanged();
                        String message = context.getResources().getString(R.string.successmessage);
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        String message = context.getResources().getString(R.string.somethingwrong);
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    String message = context.getResources().getString(R.string.connectionerror);
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                }


            }
        });
//        interfaceAcceptOffer.acceptOffer(position);
//        showdialouge(position);
    }

    @Override
    public int getItemCount()
    {
        if(list != null)
        {
            return list.size();
        }
        else
        {
            return 0;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        ImageButton btnaccept, btnreject, btnRating;
        TextView txtnote, txtname, txtwait;
        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView)itemView.findViewById(R.id.item_notification_image);
            txtnote = (TextView) itemView.findViewById(R.id.item_notification_txtnote);
            txtname = (TextView) itemView.findViewById(R.id.item_notification_txtname);
            txtwait = (TextView) itemView.findViewById(R.id.item_notification_txtwait);
            btnaccept = (ImageButton) itemView.findViewById(R.id.item_notification_btnaccept);
            btnreject = (ImageButton)itemView.findViewById(R.id.item_notification_btnreject);
            btnRating = itemView.findViewById(R.id.item_notification_btnrating);
            btnRating.setVisibility(View.GONE);
        }
    }
}
