package com.uzair.offer.price.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.uzair.offer.price.R;

import java.util.Locale;

public class StartAs extends AppCompatActivity implements View.OnClickListener {

    private ViewGroup btncompany,btncustomer;
    private SharedPreferences sharedPreferences;
    private TextView txtlanguage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_as);

        initial();
    }

    private void initial()
    {
        sharedPreferences = getSharedPreferences("user",MODE_PRIVATE);
        btncompany = (ViewGroup)findViewById(R.id.startas_company);
        btncompany.setOnClickListener(this);
        btncustomer= (ViewGroup)findViewById(R.id.startas_customer);
        btncustomer.setOnClickListener(this);
        txtlanguage= (TextView) findViewById(R.id.startas_txtlanguage);
        txtlanguage.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        if(view.getId() == btncompany.getId())
        {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean("startas",true).commit();
            startactivity(true);
        }
        else if(view.getId() == btncustomer.getId())
        {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean("startas",false).commit();
            startactivity(false);
        }
        else if ( view.getId() == txtlanguage.getId())
        {
            showdialouge();
        }
    }

    void showdialouge ()
    {
        final Dialog dialog = new Dialog(StartAs.this,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_setting);

        ImageButton btnclose = (ImageButton)dialog.findViewById(R.id.dialog_language_btnclose);
        final TextView txtarabic = (TextView)dialog.findViewById(R.id.dialog_language_arabic);
        final TextView txtenglish = (TextView)dialog.findViewById(R.id.dialog_language_english);
        setcolor(txtarabic, txtenglish);
        txtenglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                changelanguage("en");
                setcolor(txtarabic, txtenglish);
                restartapp();
            }
        });
        txtarabic.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                changelanguage("ar");
                setcolor(txtarabic, txtenglish);
                restartapp();
            }
        });
        btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }

    private void restartapp()
    {
        finish();
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage( getBaseContext().getPackageName() );
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    private void setcolor(TextView txtarabic, TextView txtenglish)
    {
        String lan = sharedPreferences.getString("lang","ar");
        if(lan.equalsIgnoreCase("en"))
        {
            txtenglish.setTextColor(Color.parseColor("#fd001e"));
            txtarabic.setTextColor(Color.BLACK);
            txtenglish.setCompoundDrawablesWithIntrinsicBounds (R.drawable.ic_lines,0,R.drawable.ic_tick,0);
            txtarabic.setCompoundDrawablesWithIntrinsicBounds (R.drawable.ic_lines,0,0,0);
        }
        else
        {
            txtarabic.setTextColor(Color.parseColor("#fd001e"));
            txtenglish.setTextColor(Color.BLACK);
            txtarabic.setCompoundDrawablesWithIntrinsicBounds (R.drawable.ic_lines,0,R.drawable.ic_tick,0);
            txtenglish.setCompoundDrawablesWithIntrinsicBounds (R.drawable.ic_lines,0,0,0);
        }
    }

    private void changelanguage(String language)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("lang",language).commit();
        String languageToLoad  = language;
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,getBaseContext().getResources().getDisplayMetrics());
    }

    private  void startactivity(boolean startas)
    {
        Intent intent = new Intent(StartAs.this,LogIn.class);
        intent.putExtra("startas",startas);
        startActivity(intent);
    }
}
